/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author KollyF01
 */
public class ExerciceEleve implements Serializable {

    private ArrayList<String> exercices;

    public ExerciceEleve() {
        exercices = new ArrayList<>();
    }

    public ArrayList<String> getExercices() {
        return exercices;
    }

    public void setExercices(ArrayList<String> exercices) {
        this.exercices = exercices;
    }

}
