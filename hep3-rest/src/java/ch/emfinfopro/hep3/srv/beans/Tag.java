/*
 * Erwan Sturzenegger
 */
package ch.emfinfopro.hep3.srv.beans;

import java.util.ArrayList;

/**
 *
 * @author Erwan Sturzenegger
 */
public class Tag {
    private Integer id;
    private String noms;
    private ArrayList<String> jeux;

    public Tag(int id, String noms) {
        this.id = id;
        this.noms = noms;
        this.jeux = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNoms() {
        return noms;
    }

    public void setNoms(String noms) {
        this.noms = noms;
    }

    public ArrayList<String> getJeux() {
        return jeux;
    }

    public void setJeux(ArrayList<String> jeux) {
        this.jeux = jeux;
    }
    
    public void addJeu(String jeu){
       this.jeux.add(jeu);
    }
            
}
