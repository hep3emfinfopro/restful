/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author KollyF01
 */
public class Jeu implements Serializable {

    private static final long serialVersionUID = 5L;
    private Integer id;
    private String nom;
    private String image;
    private String module;
    private String description;
    private String json;
    private String css;
    private String evaluation;
    private int discipline;
    private String objectif;
    private String script;
    private String scenario;
    private ArrayList<Integer> lstDegres;
    private ArrayList<Langue> lstLangues;
    private ArrayList<String> lstTags;
    private ArrayList<Sprite> lstSprites;
    private HashMap<String, String> texts;
    private Boolean inDevelopment;

    public Jeu() {
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }
    
    public String getScenario() {
        return scenario;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjectif() {
        return objectif;
    }

    public void setObjectif(String objectif) {
        this.objectif = objectif;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public int getDiscipline() {
        return discipline;
    }

    public void setDiscipline(int discipline) {
        this.discipline = discipline;
    }

    public ArrayList<Integer> getLstDegres() {
        return lstDegres;
    }

    public void setLstDegres(ArrayList<Integer> lstDegres) {
        this.lstDegres = lstDegres;
    }

    public ArrayList<Langue> getLstLangues() {
        return lstLangues;
    }

    public void setLstLangues(ArrayList<Langue> lstLangues) {
        this.lstLangues = lstLangues;
    }

    public ArrayList<String> getLstTags() {
        return lstTags;
    }

    public void setLstTags(ArrayList<String> lstTags) {
        this.lstTags = lstTags;
    }

    public ArrayList<Sprite> getLstSprites() {
        return lstSprites;
    }

    public void setLstSprites(ArrayList<Sprite> lstSprites) {
        this.lstSprites = lstSprites;
    }

    public HashMap<String, String> getTexts() {
        return texts;
    }

    public void setTexts(HashMap<String, String> texts) {
        this.texts = texts;
    }

    public Boolean getInDevelopment() {
        return inDevelopment;
    }

    public void setInDevelopment(Boolean inDevelopment) {
        this.inDevelopment = inDevelopment;
    }

    
}
