/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

import ch.emfinfopro.hep3.srv.wrk.WrkDB;

/**
 *
 * @author KollyF01
 */
public class ConnexionPool {
    public static ConnexionPool instance;
    private WrkDB db;
    
    private ConnexionPool() {
        db = new WrkDB();
    }
    
    public static synchronized ConnexionPool getInstance() {
        if (instance == null) {
            instance = new ConnexionPool();
        }
        return instance;
    }
    
    public WrkDB getWrkDB() {
        return db;
    }
}
