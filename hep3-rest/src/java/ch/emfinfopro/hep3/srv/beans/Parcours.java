/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

import java.io.Serializable;

public class Parcours implements Serializable
{
    private static final long serialVersionUID = 6L;
    private int id;
    private String nom;
    private String licence;
    private String description;
    private int degre;
    private String competences;
    private String steps;
    private String created;
    private String updated;
    private String owner;
    
    public int getId() {
        return this.id;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public String getLicence() {
        return this.licence;
    }
    
    public int getDegre() {
        return this.degre;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public String getCompetences() {
        return this.competences;
    }
    
    public String getSteps() {
        return this.steps;
    }
    
    public String getCreated() {
        return this.created;
    }
    
    public String getUpdated() {
        return this.updated;
    }
    
    public String getOwner() {
        return this.owner;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public void setNom(final String nom) {
        this.nom = nom;
    }
    
    public void setLicence(final String licence) {
        this.licence = licence;
    }
    
    public void setDegre(final int degre) {
        this.degre = degre;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
    
    public void setCompetences(final String competences) {
        this.competences = competences;
    }
    
    public void setSteps(final String steps) {
        this.steps = steps;
    }
    
    public void setCreated(final String created) {
        this.created = created;
    }
    
    public void setUpdated(final String updated) {
        this.updated = updated;
    }
    
    public void setOwner(final String owner) {
        this.owner = owner;
    }
}