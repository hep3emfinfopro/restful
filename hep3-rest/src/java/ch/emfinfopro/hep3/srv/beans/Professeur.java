/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

import java.io.Serializable;

/**
 *
 * @author KollyF01
 */
public class Professeur implements Serializable{
    
    private static final long serialVersionUID = 6L;
    private int id, role, defaultStudentId;
    private String email;
    private boolean isConfirmed;

    public Professeur() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isIsConfirmed() {
        return isConfirmed;
    }

    public void setIsConfirmed(boolean isConfirmed) {
        this.isConfirmed = isConfirmed;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
    
    public int getDefaultStudentId() {
        return defaultStudentId;
    }

    public void setDefaultStudentId(int defaultStudentId) {
        this.defaultStudentId = defaultStudentId;
    }
    
    
    
}
