/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

/**
 *
 * @author KollyF01
 */
public class Sprite {
    private String nom;
    private String base64;
    private int longueur;
    private int hauteur;
    private int frames;

    public Sprite() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public int getLongueur() {
        return longueur;
    }

    public void setLongueur(int longeur) {
        this.longueur = longeur;
    }

    public int getHauteur() {
        return hauteur;
    }

    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

    public int getFrames() {
        return frames;
    }

    public void setFrames(int frames) {
        this.frames = frames;
    }
}
