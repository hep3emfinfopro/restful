/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

import java.io.Serializable;

/**
 *
 * @author KollyF01
 */
public class Eleve implements Serializable {

    private static final long serialVersionUID = 2L;
    private int id;
    private String nom;
    private int fk_classe;
    private String classe;
    private String degre;
    private int fk_degre;
    private String padId;
    private String classPadId;


    public Eleve() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getFk_classe() {
        return fk_classe;
    }

    public void setFk_classe(int fk_classe) {
        this.fk_classe = fk_classe;
    }

    public String getDegre() {
        return degre;
    }

    public void setDegre(String degre) {
        this.degre = degre;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public int getFk_degre() {
        return fk_degre;
    }

    public void setFk_degre(int fk_degre) {
        this.fk_degre = fk_degre;
    }
    
    public String getPadId() {
        return padId;
    }

    public void setPadId(String padId) {
        this.padId = padId;
    }

    public String getClassPadId() {
        return classPadId;
    }

    public void setClassPadId(String classPadId) {
        this.classPadId = classPadId;
    }


}
