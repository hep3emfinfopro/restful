 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

import java.util.ArrayList;

/**
 *
 * @author kollyf01
 */
public class HistoriqueEleve {

    private String nom_eleve;
    private String nom_classe;
    private ArrayList<HistoriqueLine> historique;

    public HistoriqueEleve() {
    }

    public String getNom_eleve() {
        return nom_eleve;
    }

    public void setNom_eleve(String nom_eleve) {
        this.nom_eleve = nom_eleve;
    }

    public String getNom_classe() {
        return nom_classe;
    }

    public void setNom_classe(String nom_classe) {
        this.nom_classe = nom_classe;
    }

    public ArrayList<HistoriqueLine> getHistorique() {
        return historique;
    }

    public void setHistorique(ArrayList<HistoriqueLine> historique) {
        this.historique = historique;
    }

}
