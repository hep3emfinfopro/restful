/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

import java.util.ArrayList;

/**
 *
 * @author kollyf01
 */
public class HistoriqueClasse {
    private String nom_classe;
    private ArrayList<HistoriqueEleve> historique;

    public String getNom_classe() {
        return nom_classe;
    }

    public void setNom_classe(String nom_classe) {
        this.nom_classe = nom_classe;
    }

    public ArrayList<HistoriqueEleve> getHistorique() {
        return historique;
    }

    public void setHistorique(ArrayList<HistoriqueEleve> historique) {
        this.historique = historique;
    }
    
    
}
