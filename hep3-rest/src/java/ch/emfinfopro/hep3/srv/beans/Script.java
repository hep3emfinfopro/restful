/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.beans;

/**
 *
 * @author KollyF01
 */
public class Script {

    private int pk_script;
    private String script;
   
    public Script() {
    }

    public int getPk_script() {
        return pk_script;
    }

    public void setPk_script(int pk_script) {
        this.pk_script = pk_script;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

}
