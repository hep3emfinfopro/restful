package ch.emfinfopro.hep3.srv.wrk;

import ch.emfinfopro.netviewer.lib.beans.DataReturn;
import java.sql.ResultSet;
import java.sql.SQLException;
import ch.emfinfopro.hep3.srv.beans.Classe;
import ch.emfinfopro.hep3.srv.beans.ConnexionPool;
import ch.emfinfopro.hep3.srv.beans.Degre;
import ch.emfinfopro.hep3.srv.beans.Discipline;
import ch.emfinfopro.hep3.srv.beans.Eleve;
import ch.emfinfopro.hep3.srv.beans.ErrorCode;
import ch.emfinfopro.hep3.srv.beans.Exercice;
import ch.emfinfopro.hep3.srv.beans.GamesForStats;
import ch.emfinfopro.hep3.srv.beans.HistoriqueClasse;
import ch.emfinfopro.hep3.srv.beans.HistoriqueEleve;
import ch.emfinfopro.hep3.srv.beans.Jeu;
import ch.emfinfopro.hep3.srv.beans.Langue;
import ch.emfinfopro.hep3.srv.beans.Professeur;
import ch.emfinfopro.hep3.srv.beans.Script;
import ch.emfinfopro.hep3.srv.beans.Sprite;
import ch.emfinfopro.hep3.srv.beans.HistoriqueLine;
import ch.emfinfopro.hep3.srv.beans.Log;
import ch.emfinfopro.hep3.srv.beans.Parcours;
import ch.emfinfopro.hep3.srv.beans.Statistiques;
import ch.emfinfopro.hep3.srv.beans.Tag;
import ch.emfinfopro.hep3.srv.beans.User;
import ch.emfinfopro.hep3.srv.cron.CronTaskRegister;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import java.sql.Blob;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.UUID;
import org.apache.tomcat.util.codec.binary.Base64;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author carre
 */
public class Wrk {

    private WrkDB db;
    private String errDB;
    ConnexionPool connexionPool;

    public Wrk() {
        connexionPool = ConnexionPool.getInstance();
        db = connexionPool.getWrkDB();
        CronTaskRegister.register();
    }

    /**
     * Vérifie l'email et le mot de passe d'un professeur
     *
     * @param mail l'email du professeur
     * @param password le mot de passe du compte
     * @return le professeur si les crédentiels sont bon, l'élément qui ne va
     * pas sinon.
     */
    public DataReturn<Professeur> checkLogin(String mail, String password) {
        Connection conn = db.open();
        DataReturn<Professeur> out = new DataReturn();
        ResultSet rs = null;
        if (conn != null) {
            mail = mail.toLowerCase();
            try {
                rs = db.select(conn, SQLQuery.GET_PROFESSEUR_BY_USERNAME, mail);
                if (rs != null && rs.first()) {
                    Professeur p = new Professeur();
                    p.setEmail(rs.getString("email"));
                    p.setId(rs.getInt("id"));
                    p.setRole(rs.getInt("role_id"));
                    p.setDefaultStudentId(rs.getInt("default_student_id"));
                    p.setIsConfirmed(rs.getBoolean("isConfirmed"));
                    out.success(p);
                    if (BCrypt.checkpw(password, rs.getString("password"))) {
                        p.setIsConfirmed(rs.getBoolean("isConfirmed"));
                        out.success(p);
                    } else {
                        out.fail("Mauvais mot de passe", p, ErrorCode.WRONG_PASSWORD);
                    }
                } else {
                    out.fail("Utilisateur inexistant", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_user");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs est deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Récupère les données d'un professeur à partir de son Id
     *
     * @param id l'id du professeur
     * @return le professeur, l'élément qui ne va pas sinon.
     */
    public DataReturn<Professeur> getTeacherById(int id) {
        Connection conn = db.open();
        DataReturn<Professeur> out = new DataReturn();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_TEACHER_BY_ID, id);
                if (rs != null && rs.first()) {
                    Professeur p = new Professeur();
                    p.setEmail(rs.getString("email"));
//                    p.setId(rs.getInt("id"));
//                    p.setRole(rs.getInt("role_id"));
                    p.setDefaultStudentId(rs.getInt("default_student_id"));
                    out.success(p);
                } else {
                    out.fail("Utilisateur inexistant", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_user");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs est deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Crée un nouveau compte pour un professeur
     *
     * @param mail l'email du professeur
     * @param password le mot de passe du compte
     * @param code le code de confirmation
     * @return le professeur si le compte a bien été créé, le problème autrement
     */
    public DataReturn<Professeur> createUser(String mail, String password, String code) {
        Connection conn = db.open();
        DataReturn<Professeur> out = new DataReturn();
        ResultSet rs = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        if (conn != null) {
            mail = mail.toLowerCase();
            try {
                // Code désactivé pour ne plus restreindre l'accès à certains domaines
//                rs2 = db.select(conn, SQLQuery.GET_EMAILS);
//                boolean isAdresseValid = false;
//                while (rs2 != null && rs2.next()) {
//                    if (mail.substring(mail.indexOf("@"), mail.length()).equals(rs2.getString("address"))) {
//                        isAdresseValid = true;
//                        break;
//                    }
//                }
//                if (isAdresseValid) {
                String hashed = BCrypt.hashpw(password, BCrypt.gensalt(8));
//                  On insère un compte élève par défaut pour que le professeur puisse faire des test sur les jeux à son compte
//                  Par convention, le nom de l'élève sera le mail du professeur et il appartiendra à la classe 116
                rs3 = db.insert(conn, SQLQuery.INSERT_STUDENT, mail, 116);
//                  Si tout se passe bien, on insère l'utilisateur avec l'id de son compte élève
                if (rs3 != null && rs3.first()) {
//                  Le dernier paramètre correspond à l'id créé automatiquement dans t_student et utilisé dans t_user comme clé étrangère (t_default_student)
                    rs = db.insert(conn, SQLQuery.INSERT_USER, mail, hashed, code, rs3.getInt("id"));
                    if (rs != null && rs.first()) {
                        Professeur p = new Professeur();
                        p.setId(rs.getInt("id"));
                        p.setEmail(mail);
                        p.setIsConfirmed(false);
                        out.success(p);
                    } else {
                        out.fail("Impossible d'ajouter l'utilisateur", ErrorCode.DATABASE_DUPLICATE);
                        out.setAffectedTables("t_user");
                    }
                }
// Code désactivé pour ne plus restreindre l'accès à certains domaines
//                } else {
//                    out.fail("L'adresse n'est pas valide", ErrorCode.INVALID_ENTRY);
//                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                    if (rs2 != null && !rs2.isClosed()) {
                        rs2.close();
                    }
                } catch (SQLException ex) {
                    // rs est deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Confirme un utilisateur via son code
     *
     * @param code le code de l'utilisateur à confirmer
     * @return 1 si tout se passe bien, le problème autrement
     */
    public DataReturn<Integer> confirmUser(String code) {
        Connection conn = db.open();
        DataReturn<Integer> out = new DataReturn();
        int result;
        if (conn != null) {
            try {
                result = db.update(conn, SQLQuery.UPDATE_CONFIRM_USER, code);
                if (result == 1) {
                    out.success(result);
                } else {
                    out.fail("Impossible de confirmer l'utilisateur", ErrorCode.INVALID_ENTRY);
                    out.setAffectedTables("t_user");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient les classes d'un professeur
     *
     * @param id la PK du professeur
     * @return la liste des classes du professeur si tout va bien, le problème
     * autrement
     */
    public DataReturn<ArrayList> getClasses(int id) {
        Connection conn = db.open();
        DataReturn<ArrayList> out = new DataReturn<>();
        ArrayList<Classe> list = new ArrayList<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_CLASSES_BY_USER, id);
                if (rs != null) {
                    while (rs.next()) {
                        Classe c = new Classe();
                        c.setNom(rs.getString("name"));
                        c.setId(rs.getInt("id"));
                        c.setDegre(rs.getString("degree"));
                        c.setUuid(rs.getString("uuid"));
                        list.add(c);
                    }
                    out.success(list);
                } else {
                    out.fail("Impossible d'obtenir les classes", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_class");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs est deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient la classe via sa PK
     *
     * @param id la PK de la classe
     * @return la classe dont la PK est indiquée si tout va bien, le problème
     * autrement
     */
    public DataReturn<Classe> getClasse(int id) {
        Connection conn = db.open();
        DataReturn<Classe> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_CLASS_BY_ID, id);
                if (rs != null && rs.first()) {
                    Classe c = new Classe();
                    c.setNom(rs.getString("name"));
                    c.setId(rs.getInt("t_class.id"));
                    c.setDegre(rs.getString("degree"));
                    c.setUuid(rs.getString("uuid"));
                    out.success(c);
                } else {
                    out.fail("Impossible d'obtenir la classe", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_class");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs est deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient la liste des élèves d'un professeur
     *
     * @param id la PK du professeur
     * @return la liste des élèves qui se trouvent dans une des classes du
     * professeur si tout va bien, le problème autrement
     */
    public DataReturn<ArrayList> getEleves(int id) {
        Connection conn = db.open();
        DataReturn<ArrayList> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_STUDENTS_BY_CLASS, id);
                if (rs != null) {
                    ArrayList<Eleve> list = new ArrayList<>();
                    while (rs.next()) {
                        Eleve e = new Eleve();
                        e.setNom(rs.getString("name"));
                        e.setId(rs.getInt("student_id"));
                        e.setFk_classe(rs.getInt("class_id"));
                        e.setDegre(rs.getString("degree"));
                        e.setClasse(rs.getString("class"));
                        e.setPadId(rs.getString("student_pad_id"));
                        e.setClassPadId(rs.getString("class_pad_id"));
                        list.add(e);
                    }
                    out.success(list);
                } else {
                    out.fail("Impossible d'obtenir les élèves", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_student");
                }

            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs est deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient la liste des parcours d'un professeur
     *
     * @param user_id la PK du professeur
     * @return la liste des élèves qui se trouvent dans une des classes du
     * professeur si tout va bien, le problème autrement
     */
    public DataReturn<ArrayList> getParcours(int user_id) {
        Connection conn = db.open();
        DataReturn<ArrayList> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_PARCOURS_BY_USER_ID, user_id);
                if (rs != null) {
                    ArrayList<Parcours> list = new ArrayList<>();
                    while (rs.next()) {
                        Parcours p = new Parcours();
                        p.setId(rs.getInt("id"));
                        p.setNom(rs.getString("name"));
                        p.setLicence(rs.getString("licence"));
                        p.setDegre(rs.getInt("degre_id"));
                        p.setDescription(rs.getString("description"));
                        p.setCompetences(rs.getString("competences"));
                        p.setSteps(rs.getString("steps"));
                        p.setCreated(rs.getString("created"));
                        p.setUpdated(rs.getString("updated"));
                        list.add(p);
                    }
                    out.success(list);
                } else {
                    out.fail("Impossible d'obtenir les parcours", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_parcours");
                }

            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs est deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient la liste des parcours pour une classe ou un élève donné(e)
     *
     * @param target l'élément ciblé : classe ou élève
     * @param target_id l'id de la classe voulue
     * @param user_id la PK du professeur
     * @return la liste des élèves qui se trouvent dans une des classes du
     * professeur si tout va bien, le problème autrement
     */
    public DataReturn<ArrayList> getAllParcoursOfTarget(String target, int target_id) {
        Connection conn = db.open();
        DataReturn<ArrayList> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                if (target.equals("class")) {
                    rs = db.select(conn, SQLQuery.GET_PARCOURS_BY_CLASS_ID, target_id);
                } else if (target.equals("student")) {
                    rs = db.select(conn, SQLQuery.GET_PARCOURS_BY_STUDENT_ID, target_id);
                }
                if (rs != null) {
                    ArrayList<Parcours> list = new ArrayList<>();
                    while (rs.next()) {
                        Parcours p = new Parcours();
                        p.setId(rs.getInt("id"));
                        p.setNom(rs.getString("name"));
//                        p.setLicence(rs.getString("licence"));
//                        p.setDegre(rs.getInt("degre_id"));
//                        p.setDescription(rs.getString("description"));
//                        p.setCompetences(rs.getString("competences"));
//                        p.setSteps(rs.getString("steps"));
//                        p.setCreated(rs.getString("created"));
//                        p.setUpdated(rs.getString("updated"));
                        list.add(p);
                    }
                    out.success(list);
                } else {
                    out.fail("Impossible d'obtenir les parcours", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_parcours");
                }

            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs est deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient les informations d'un seul parcours à partir de son id
     *
     * @param parcours_id l'id du parcours
     * @return le parcours demandé parcours si tout va bien, le problème
     * autrement
     */
    public DataReturn<Parcours> getSingleParcoursById(int parcours_id) {
        Connection conn = db.open();
        DataReturn<Parcours> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_SINGLE_PARCOURS_BY_ID, parcours_id);

                if (rs != null && rs.first()) {
                    Parcours p = new Parcours();
                    p.setId(rs.getInt("id"));
                    p.setNom(rs.getString("name"));
                    p.setLicence(rs.getString("licence"));
                    p.setDegre(rs.getInt("degre_id"));
                    p.setDescription(rs.getString("description"));
                    p.setCompetences(rs.getString("competences"));
                    p.setSteps(rs.getString("steps"));
                    p.setCreated(rs.getString("created"));
                    p.setUpdated(rs.getString("updated"));
                    out.success(p);
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient les informations d'un seul parcours à partir du userId et du
     * nomParcours (utile si on n'a pas l'id)
     *
     * @param user_id l'id de l'utilisateur / le prof
     * @param nomParcours le nom du parcours (unique pour chauqe userId)
     * @return le parcours demandé parcours si tout va bien, le problème
     * autrement
     */
    public DataReturn<Parcours> getSingleParcoursByUserAndName(int user_id, String nomParcours) {
        Connection conn = db.open();
        DataReturn<Parcours> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_SINGLE_PARCOURS_BY_USER_AND_NAME, user_id, nomParcours);

                if (rs != null && rs.first()) {
                    Parcours p = new Parcours();
                    p.setId(rs.getInt("id"));
                    p.setNom(rs.getString("name"));
                    p.setLicence(rs.getString("licence"));
                    p.setDegre(rs.getInt("degre_id"));
                    p.setDescription(rs.getString("description"));
                    p.setCompetences(rs.getString("competences"));
                    p.setSteps(rs.getString("steps"));
                    p.setCreated(rs.getString("created"));
                    p.setUpdated(rs.getString("updated"));
                    out.success(p);
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    
    /**
     * Obtient les étapes personnalisés d'un parcours pour un élève en particulier
     * @param student_id l'id de l'élève
     * @param parcours_id l'id du parcours
     * @return le parcours avec les étapes si tout va bien, le problème autrement
     */
    public DataReturn<Parcours> getStudentCustomStepsOfParcours(int student_id, int parcours_id) {
        Connection conn = db.open();
        DataReturn<Parcours> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_STUDENT_CUSTOM_STEPS_OF_PARCOURS, student_id, parcours_id);
                if (rs != null && rs.first()) {
                    Parcours p = new Parcours();                    
                    p.setSteps(rs.getString("custom_steps"));                    
                    out.success(p);
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }
    
    
    /**
     * Vérifie que le parcours existe pour une classe ou un élève donnée
     *
     * @param target élément ciblé : classe ou élève
     * @param target_id l'id de la classe ou de l'élève en question
     * @param parcours_id l'id du parcours
     * @return le parcours demandé (pour l'instant vide car pas besoin de plus)
     * parcours si tout va bien, le problème autrement
     */
    public DataReturn<Parcours> checkParcoursFor(String target, int target_id, int parcours_id) {
        Connection conn = db.open();
        DataReturn<Parcours> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                if (target.equals("class")) {
                    rs = db.select(conn, SQLQuery.GET_PARCOURS_BY_CLASS_ID_AND_PARCOURS_ID, target_id, parcours_id);
                } else if (target.equals("student")) {
                    rs = db.select(conn, SQLQuery.GET_PARCOURS_BY_STUDENT_ID_AND_PARCOURS_ID, target_id, parcours_id);
                }

                if (rs != null && rs.first()) {
                    // On ne récupère pas beaucoup d'informations parce qu'on veut juste faire un check
                    Parcours p = new Parcours();
                    out.success(p);
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Vérifie combien de fois un niveau a été fait dans un parcours pour un
     * élève donné
     *
     * @param studentId l'id de l'élève
     * @param parcoursId l'id du parcours
     * @param exoId l'id de l'exercice
     * @return le nombre de lignes trouvé count si tout va bien, le problème
     * autrement
     */
    public DataReturn<String> checkStepDoneForStudent(int studentId, int parcoursId, int exoId) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.COUNT_STEPDONE_IN_PARCOURS_FOR_STUDENT, studentId, parcoursId, exoId);
                if (rs != null && rs.first()) {
                    int count = rs.getInt(1); // Récupère la valeur de la première colonne            
                    out.success(Integer.toString(count));
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient l'élève via sa PK
     *
     * @param id la PK de l'élève
     * @return l'élève si tout se passe bien, le problème autrement
     */
    public DataReturn<Eleve> getEleve(int id) {
        Connection conn = db.open();
        DataReturn<Eleve> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_STUDENT_BY_ID, id);
                if (rs != null && rs.first()) {
                    Eleve e = new Eleve();
                    e.setNom(rs.getString("name"));
                    e.setId(rs.getInt("id"));
                    e.setFk_classe(rs.getInt("class_id"));
                    e.setDegre(rs.getString("degree"));
                    e.setClasse(rs.getString("class"));
                    e.setFk_degre(rs.getInt("degree_id"));
                    e.setClassPadId(rs.getString("class_pad_id"));
                    e.setPadId(rs.getString("student_pad_id"));
                    out.success(e);
                } else {
                    out.fail("Impossible d'obtenir l'élève", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_student");
                }

            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Ajoute une classe pour un professeur, avec des élèves
     *
     * @param nom le nom de la classe
     * @param degre le degrès Harmos de la classe
     * @param id la PK du professeur
     * @param eleves le String contenant les noms des élèves à ajouter dans la
     * classe, séparés par un %
     * @return la classe ajoutée si tout se passe bien, le problème autrement
     */
    public DataReturn<Classe> addClasse(String nom, int degre, int id, List<String> eleves) {
        Connection conn = db.open();
        DataReturn<Classe> out = new DataReturn();
        ResultSet rs = null;
        ResultSet rs2 = null;
        if (conn != null) {
            try {
                conn.setAutoCommit(false);
                String uuid = String.valueOf(UUID.randomUUID());
                rs = db.insert(conn, SQLQuery.INSERT_CLASS, nom, uuid, degre);
                if (rs != null && rs.first()) {
                    rs2 = db.insert(conn, SQLQuery.INSERT_CLASS_TO_USER, id, rs.getInt("id"));
                    if (rs2 != null && rs.first()) {
                        Classe c = new Classe();
                        c.setId(rs.getInt("id"));
                        c.setNom(rs.getString("name"));
                        c.setUuid(rs.getString("uuid"));
                        if (eleves != null) {
                            for (String eleve : eleves) {
                                rs2 = db.insert(conn, SQLQuery.INSERT_STUDENT, eleve, rs.getInt("id"));
                            }
                        }
                        out.success(c);
                        conn.commit();
                    } else {
                        out.fail("Impossible d'ajouter la classe au professeur");
                        out.setAffectedFields("tr_user_class");
                        conn.rollback();
                    }
                } else {
                    out.fail("Impossible d'ajouter la classe", ErrorCode.DATABASE_DUPLICATE);
                    out.setAffectedTables("t_class");
                    conn.rollback();
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
                try {
                    conn.rollback();
                } catch (SQLException ex1) {
                    // rien a faire
                }
            } finally {
                try {
                    conn.setAutoCommit(true);
                } catch (SQLException ex) {
                    // rien a faire
                }
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                    if (rs2 != null && !rs2.isClosed()) {
                        rs2.close();
                    }
                } catch (SQLException ex) {
                    // rs est deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Ajoute un élève dans une classe
     *
     * @param nom le nom de l'élève
     * @param classe la PK de la classe dans laqelle ajouter l'élève
     * @return l'élève si tout se passe bien, le problème autrement
     */
    public DataReturn<Eleve> addEleve(String nom, int classe) {
        Connection conn = db.open();
        DataReturn<Eleve> out = new DataReturn();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.insert(conn, SQLQuery.INSERT_STUDENT, nom, classe);
                if (rs != null && rs.first()) {
                    Eleve e = new Eleve();
                    e.setId(rs.getInt("id"));
                    e.setNom(rs.getString("name"));
                    e.setFk_classe(rs.getInt("class_id"));
                    e.setPadId(rs.getString("pad_id"));
                    out.success(e);
                } else {
                    out.fail("Impossible d'ajouter l'élève", ErrorCode.DATABASE_DUPLICATE);
                    out.setAffectedTables("t_student");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Ajoute un nouveau parcours
     *
     * @param user_id l'id de l'utilisateur (teacherId)
     * @param nom le nom du parcours
     * @param targetSkills les compétences cibles choisies pour ce parcours
     * @return l'id du parcours si tout se passe bien
     */
    public DataReturn<String> createParcours(int user_id, String nom, String targetSkills) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        int id = 0;
        if (conn != null) {
            try {
                ResultSet generatedKey = db.insert(conn, SQLQuery.CREATE_PARCOURS, user_id, nom, targetSkills);
                if (generatedKey.first()) {
                    id = generatedKey.getInt(1);
                }
                out.success(Integer.toString(id));
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Ajoute un parcours
     *
     * @param user_id l'id de l'utilisateur (teacherId) : compose la PK
     * @param nom le nom du parcours : compose la PK
     * @param licence la licence
     * @param description la description
     * @param degre le degre Harmos du parcours
     * @param competences les competences
     * @param steps les étapes du parcours (en un seul string)
     * @return le parcours si tout se passe bien, le problème autrement
     */
    public DataReturn<Parcours> saveParcours(int user_id, String nom, String licence, String description, int degre, String competences, String steps) {
        Connection conn = db.open();
        DataReturn<Parcours> out = new DataReturn();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.insert(conn, SQLQuery.INSERT_PARCOURS, user_id, nom, licence, description, degre, competences, steps);
                if (rs != null && rs.first()) {
                    Parcours p = new Parcours();
                    p.setId(rs.getInt("user_id"));
                    // p.setNom(rs.getString("name"));
                    //p.setFk_user(rs.getInt("user_id"));
                    out.success(p);
                } else {
                    out.fail("Impossible de créer le parcours", ErrorCode.DATABASE_DUPLICATE);
                    out.setAffectedTables("t_parcours");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Ajoute un parcours pour une classe
     *
     * @param target pour savoir si on fait la requete sur une classe ou sur un élève
     * @param target_id l'id de la cible (classe/élève) a qui on va assigner le parcours     
     * @param parcours_id l'id du parcours
     * @param initialSteps les étapes initiales du parcours
     * @return le parcours si tout se passe bien, le problème autrement
     */
    public DataReturn<Parcours> saveParcoursTo(String target, int target_id, int parcours_id, String initialSteps) {
        Connection conn = db.open();
        DataReturn<Parcours> out = new DataReturn();
        ResultSet rs = null;
        if (conn != null) {
            try {
                if (target.equals("class")) {
                    rs = db.insert(conn, SQLQuery.INSERT_PARCOURS_TO_CLASS, target_id, parcours_id);
                } else if (target.equals("student")) {
                    rs = db.insert(conn, SQLQuery.INSERT_PARCOURS_TO_STUDENT, target_id, parcours_id, initialSteps);
                }
                if (rs != null && rs.first()) {
                    Parcours p = new Parcours();
                    p.setId(rs.getInt("user_id"));
                    // p.setNom(rs.getString("name"));
                    //p.setFk_user(rs.getInt("user_id"));
                    out.success(p);
                } else {
                    out.fail("Impossible de créer le parcours", ErrorCode.DATABASE_DUPLICATE);
                    out.setAffectedTables("t_parcours");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Met à jour un parcours existant
     *
     * @param user_id l'id de l'utilisateur (teacherId) : compose la PK
     * @param nom le nom du : compose la PK
     * @param licence la licence
     * @param description la description
     * @param competences les competences ne changent plus dans les metadata
     * mais je laisse le paramètre au cas où il y aura des compétences
     * détaillées qui vont changer
     * @param steps les étapes du parcours (en un seul string)
     * @return le parcours si tout se passe bien, le problème autrement
     */
    public DataReturn<Parcours> updateParcours(int user_id, String nom, String licence, String description, String steps) {
        Connection conn = db.open();
        DataReturn<Parcours> out = new DataReturn();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.insert(conn, SQLQuery.UPDATE_PARCOURS, licence, description, steps, user_id, nom);
                if (rs != null && rs.first()) {
                    Parcours p = new Parcours();
                    p.setId(rs.getInt("user_id"));
                    // p.setNom(rs.getString("name"));
                    //p.setFk_user(rs.getInt("user_id"));
                    out.success(p);
                } else {
                    out.fail("Impossible de modifier le parcours", ErrorCode.DATABASE_DUPLICATE);
                    out.setAffectedTables("t_parcours");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Met à jour les MetaData d'un parcours
     *
     * @param title Le nom du parcours
     * @param description La description du parcours
     * @param licence La licence utilisée pour créer le parcours
     * @param degre Le degre Harmos du parcours
     * @param tag Le(s) tag(s) associés au parcours
     * @param parcoursId L'id du parcours en question
     */
    public DataReturn<String> updateParcoursMetaData(String title, String description, String licence, String degre, String tag, int parcoursId) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        int result;
        if (conn != null) {
            try {
                result = db.update(conn, SQLQuery.UPDATE_PARCOURS_META_DATA, title, description, licence, degre, tag, parcoursId);
                if (result == 1) {
                    out.success("OK");
                } else {
                    out.fail("Impossible de modifier le parcours", ErrorCode.INVALID_ENTRY);
                    out.setAffectedTables("t_parcours");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Modifie le statut d'un parcours (public/privé)
     *
     * @param id l'id du parcours
     * @param status la valeur du statut : 0=privé, 1=en attente de publication,
     * 2=public.
     * @return le parcours si tout se passe bien, le problème autrement
     */
    public DataReturn<String> updateParcoursStatus(int id, int status) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        int result;
        if (conn != null) {
            try {
                result = db.update(conn, SQLQuery.UPDATE_PARCOURS_STATUS, status, id);
                if (result == 1) {
                    out.success("OK");
                } else {
                    out.fail("Impossible de modifier le parcours", ErrorCode.INVALID_ENTRY);
                    out.setAffectedTables("t_parcours");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Met à jour l'état de l'étape dans le json
     *
     * @param parcours_id l'id du parcours à mettre à jour
     * @param steps_json le json des étapes du parcours
     * @return un message de succès, le problème autrement
     */
    public DataReturn<String> updateStepsJson(int parcours_id, String steps_json) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        int result;
        if (conn != null) {
            try {
                result = db.update(conn, SQLQuery.UPDATE_PARCOURS_STEPS_JSON, steps_json, parcours_id);
                if (result == 1) {
                    out.success("OK");
                } else {
                    out.fail("Impossible de modifier le le json des étapes", ErrorCode.INVALID_ENTRY);
                    out.setAffectedTables("t_parcours");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }
    
    /**
     * Met à jour les étapes d'un parcours personnalisé (adpative learning)
     *
     * @param student_id l'id de l'élève
     * @param custom_parcours_id l'id du custom_parcours à mettre à jour
     * @param custom_steps_json le json des étapes du parcours
     * @return un message de succès, le problème autrement
     */
    public DataReturn<String> updateCustomStepsJson(int student_id, int custom_parcours_id, String custom_steps_json) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        int result;
        if (conn != null) {
            try {
                result = db.update(conn, SQLQuery.UPDATE_CUSTOM_STEPS_JSON, custom_steps_json, student_id, custom_parcours_id);
                if (result == 1) {
                    out.success("OK");
                } else {
                    out.fail("Impossible de modifier le le json des étapes", ErrorCode.INVALID_ENTRY);
                    out.setAffectedTables("t_parcours");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<Log> addLog(String label, String methode, String text) {
        Connection conn = db.open();
        DataReturn<Log> out = new DataReturn();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.insert(conn, SQLQuery.INSERT_LOG, label, methode, text);
                if (rs != null && rs.first()) {
                    Log l = new Log();
                    l.setNom(label);
                    l.setNom(methode);
                    l.setNom(text);
                    out.success(l);
                } else {
                    out.fail("Impossible d'ajouter le log", ErrorCode.DATABASE_DUPLICATE);
                    out.setAffectedFields("t_log");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException e) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient la liste des degrès Harmos
     *
     * @return la liste des degrès Harmos si tout se passe bien, le problème
     * autrement
     */
    public DataReturn<ArrayList> getDegres() {
        Connection conn = db.open();
        DataReturn<ArrayList> out = new DataReturn<>();
        ArrayList<Degre> list = new ArrayList<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_DEGREES);
                if (rs != null) {
                    while (rs.next()) {
                        Degre d = new Degre();
                        d.setDegre(rs.getString("degree"));
                        d.setId(rs.getInt("id"));
                        list.add(d);
                    }
                    out.success(list);
                } else {
                    out.fail("Impossible d'obtenir les degrés", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_degree");
                }

            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient la liste des disciplines
     *
     * @return la liste des disciplines si tout se passe bien, le problème
     * autrement
     */
    public DataReturn<ArrayList> getDisciplines() {
        Connection conn = db.open();
        DataReturn<ArrayList> out = new DataReturn<>();
        ArrayList<Discipline> list = new ArrayList<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_DISCIPLINES);
                if (rs != null) {
                    while (rs.next()) {
                        Discipline d = new Discipline();
                        d.setDiscipline(rs.getString("name"));
                        d.setId(rs.getInt("id"));
                        list.add(d);
                    }
                    out.success(list);
                } else {
                    out.fail("Impossible d'obtenir les disciplines", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_discipline");
                }

            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient la liste des langues
     *
     * @return la liste des langues si tout se passe bien, le problème autrement
     */
    public DataReturn<ArrayList> getLangues() {
        Connection conn = db.open();
        DataReturn<ArrayList> out = new DataReturn<>();
        ArrayList<Langue> list = new ArrayList<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_LANGUAGES);
                if (rs != null) {
                    while (rs.next()) {
                        Langue langue = new Langue();
                        langue.setPk_langue(rs.getInt("id"));
                        langue.setNom(rs.getString("name"));
                        langue.setAbbr(rs.getString("abbr"));
                        list.add(langue);
                    }
                    out.success(list);
                } else {
                    out.fail("Impossible d'obtenir les langues", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_languages");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient la liste des emails
     *
     * @return la liste des emails si tout se passe bien, le problème autrement
     */
    public DataReturn<ArrayList> getEmails() {
        Connection conn = db.open();
        DataReturn<ArrayList> out = new DataReturn<>();
        ArrayList<String> list = new ArrayList<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_EMAILS);
                if (rs != null) {
                    while (rs.next()) {
                        list.add(rs.getString("address"));
                    }
                    out.success(list);
                } else {
                    out.fail("Impossible d'obtenir les emails", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_email");
                }

            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient la liste des tags
     *
     * @return la liste des tags si tout se passe bien, le problème autrement
     */
    public DataReturn<ArrayList> getTags() {
        Connection conn = db.open();
        DataReturn<ArrayList> out = new DataReturn<>();
        ArrayList<String> list = new ArrayList<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_TAGS);
                if (rs != null) {
                    while (rs.next()) {
                        list.add(rs.getString("tag"));
                    }
                    out.success(list);
                } else {
                    out.fail("Impossible d'obtenir les tags", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_tag");
                }

            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient la liste des jeux
     *
     * @return la liste des jeux si tout se passe bien, le problème autrement
     */
    public DataReturn<ArrayList> getJeux() {
        Connection conn = db.open();
        DataReturn<ArrayList> out = new DataReturn<>();
        ArrayList<Jeu> list = new ArrayList<>();
        ResultSet rs = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        ResultSet rs4 = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_GAMES);
                if (rs != null) {
                    while (rs.next()) {
                        Jeu j = new Jeu();
                        j.setId(rs.getInt("id"));
                        j.setNom(rs.getString("name"));
                        Blob b = rs.getBlob("image");
                        int length = (int) b.length();
                        byte[] img = b.getBytes(1, length);
                        byte[] base64 = Base64.encodeBase64(img);
                        j.setImage(new String(base64));
                        j.setDescription(rs.getString("description"));
                        j.setObjectif(rs.getString("goal"));
                        j.setDiscipline(rs.getInt("discipline_id"));
                        j.setInDevelopment(rs.getBoolean("inDevelopment"));
                        j.setModule(rs.getString("module"));
                        ArrayList<Integer> lstDegre = new ArrayList<>();
                        rs2 = db.select(conn, SQLQuery.GET_DEGREES_BY_GAME, j.getId());
                        while (rs2.next()) {
                            lstDegre.add(rs2.getInt("id"));
                        }
                        j.setLstDegres(lstDegre);
                        ArrayList<Langue> lstLangue = new ArrayList<>();
                        rs3 = db.select(conn, SQLQuery.GET_LANGUAGES_BY_GAME, j.getId());
                        while (rs3.next()) {
                            Langue langue = new Langue();
                            langue.setPk_langue(rs3.getInt("id"));
                            langue.setNom(rs3.getString("name"));
                            langue.setAbbr(rs3.getString("abbr"));
                            lstLangue.add(langue);
                        }
                        j.setLstLangues(lstLangue);
                        ArrayList<String> lstTags = new ArrayList<>();
                        rs4 = db.select(conn, SQLQuery.GET_TAGS_BY_GAME, j.getId());
                        while (rs4.next()) {
                            lstTags.add(rs4.getString("tag"));
                        }
                        j.setLstTags(lstTags);
                        list.add(j);
                    }
                    out.success(list);
                } else {
                    out.fail("Impossible d'obtenir les jeux", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_game");
                }

            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                    if (rs2 != null && !rs2.isClosed()) {
                        rs2.close();
                    }
                    if (rs3 != null && !rs3.isClosed()) {
                        rs3.close();
                    }
                    if (rs4 != null && !rs4.isClosed()) {
                        rs4.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Obtient le jeu via sa PK
     *
     * @param id la PK du jeu à récupérer
     * @return le jeu souhaité si tout se passe bien, le problème aurement
     */
    public DataReturn<Jeu> getJeu(int id) {
        Connection conn = db.open();
        DataReturn<Jeu> out = new DataReturn<>();
        ResultSet rsJeu = null;
        ResultSet rsDegres = null;
        ResultSet rsLangues = null;
        ResultSet rsScripts = null;
        ResultSet rsSprites = null;
        ResultSet rsTexts = null;
        ResultSet rsScenario = null;
        if (conn != null) {
            try {
                rsJeu = db.select(conn, SQLQuery.GET_GAME_BY_ID, id);
                if (rsJeu != null) {
                    Jeu j = new Jeu();
                    if (rsJeu.first()) {
                        j.setId(rsJeu.getInt("id"));
                        j.setNom(rsJeu.getString("name"));
                        j.setModule(rsJeu.getString("module"));
                        j.setJson(rsJeu.getString("inputs"));
                        j.setCss(rsJeu.getString("css"));
                        j.setDiscipline(rsJeu.getInt("discipline_id"));
                        j.setScript(rsJeu.getString("script"));
                        j.setEvaluation(rsJeu.getString("evaluation"));
                        rsDegres = db.select(conn, SQLQuery.GET_DEGREES_BY_GAME, j.getId());
                        ArrayList<Integer> lstDegres = new ArrayList<>();
                        while (rsDegres != null && rsDegres.next()) {
                            lstDegres.add(rsDegres.getInt("id"));
                        }
                        j.setLstDegres(lstDegres);
                        ArrayList<Langue> lstLangues = new ArrayList<>();
                        rsLangues = db.select(conn, SQLQuery.GET_LANGUAGES_BY_GAME, j.getId());
                        while (rsLangues != null && rsLangues.next()) {
                            Langue langue = new Langue();
                            langue.setPk_langue(rsLangues.getInt("id"));
                            langue.setNom(rsLangues.getString("name"));
                            langue.setAbbr(rsLangues.getString("abbr"));
                            lstLangues.add(langue);
                        }
                        j.setLstLangues(lstLangues);
                        ArrayList<Sprite> lstSprites = new ArrayList<>();
                        rsSprites = db.select(conn, SQLQuery.GET_SPRITES_BY_GAME, j.getId());
                        while (rsSprites != null && rsSprites.next()) {
                            Sprite spr = new Sprite();
                            spr.setNom(rsSprites.getString("name"));
                            spr.setLongueur(rsSprites.getInt("width"));
                            spr.setHauteur(rsSprites.getInt("height"));
                            spr.setFrames(rsSprites.getInt("frames"));
                            Blob b = rsSprites.getBlob("image");
                            int length = (int) b.length();
                            byte[] img = b.getBytes(1, length);
                            byte[] base64 = Base64.encodeBase64(img);
                            spr.setBase64(new String(base64));
                            lstSprites.add(spr);
                        }
                        j.setLstSprites(lstSprites);
                        rsTexts = db.select(conn, SQLQuery.GET_TEXTS_BY_GAME, j.getId());
                        HashMap<String, String> lstTexts = new HashMap<>();
                        while (rsTexts != null && rsTexts.next()) {
                            lstTexts.put(rsTexts.getString("abbr"), rsTexts.getString("translation"));
                        }
                        j.setTexts(lstTexts);

                        rsScenario = db.select(conn, SQLQuery.GET_SCENARIO_BY_GAME, j.getId());
                        if (rsScenario != null && rsScenario.first()) {
                            j.setScenario(rsScenario.getString("scenario"));
                        }
                        out.success(j);
                    }
                } else {
                    out.fail("Impossible d'obtenir le jeu", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_jeu");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rsJeu != null && !rsJeu.isClosed()) {
                        rsJeu.close();
                    }
                    if (rsDegres != null && !rsDegres.isClosed()) {
                        rsDegres.close();
                    }
                    if (rsLangues != null && !rsLangues.isClosed()) {
                        rsLangues.close();
                    }
                    if (rsScripts != null && !rsScripts.isClosed()) {
                        rsScripts.close();
                    }
                    if (rsSprites != null && !rsSprites.isClosed()) {
                        rsSprites.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Modifie le nom d'un élève
     *
     * @param id la Pk de l'élève à modifier
     * @param name le nouveau nom
     * @return l'élève avec son nouveau nom
     */
    public DataReturn<Eleve> updateEleve(int id, String name) {
        Connection conn = db.open();
        DataReturn<Eleve> out = new DataReturn<>();
        int result;
        ResultSet rs = null;
        if (conn != null) {
            try {
                result = db.update(conn, SQLQuery.UPDATE_STUDENT, name, id);
                if (result == 1) {
                    rs = db.select(conn, SQLQuery.GET_STUDENT_BY_ID, id);
                    if (rs != null && rs.first()) {
                        Eleve e = new Eleve();
                        e.setId(rs.getInt("id"));
                        e.setNom(rs.getString("name"));
                        e.setFk_classe(rs.getInt("class_id"));
                        e.setDegre(rs.getString("degree"));
                        e.setClasse(rs.getString("class"));
                        out.success(e);
                    } else {
                        out.fail("Impossible de modifier cet élève", ErrorCode.INVALID_ENTRY);
                        out.setAffectedTables("t_student");
                    }
                } else {
                    out.fail("Impossible d'obtenir cet élève", ErrorCode.NOT_FOUND);
                    out.setAffectedTables("t_student");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Ajoute un code de récupération au professeur avec l'email indiqué
     *
     * @param email l'email du professeur à qui ajouter le code
     * @param code le code à ajouter
     * @return un DataReturn avec le String OK si tout se passe, le problème
     * autrement
     */
    public DataReturn<String> addCodeToProfesseur(String email, String code) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        int result;
        if (conn != null) {
            try {
                result = db.update(conn, SQLQuery.UPDATE_USER_CODE_BY_EMAIL, code, email);
                if (result == 1) {
                    out.success("OK");
                } else {
                    out.fail("Impossible de modifier ce professeur", ErrorCode.INVALID_ENTRY);
                    out.setAffectedTables("t_user");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Vérifie le code de récupération pour un professeur
     *
     * @param code le code entré par le professeur
     * @param email l'email du professeur entrant le code
     * @return un DataReturn avec le String OK si tout se passe, le problème
     * autrement
     */
    public DataReturn<String> verifyCodeForRecovery(String code, String email) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_CODE_FROM_USER, code, email);
                if (rs != null) {
                    if (rs.first()) {
                        out.success("OK");
                    } else {
                        out.fail("Code invalide", ErrorCode.INVALID_ENTRY);
                    }
                } else {
                    out.fail("L'adresse ne semble pas être correcte", ErrorCode.INVALID_ENTRY);
                    out.setAffectedTables("t_user");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Modifie le mot de passe du professeur avec l'email indiqué
     *
     * @param email l'email du professeur
     * @param password le nouveau mot de passe
     * @return un DataReturn avec le String OK si tout se passe, le problème
     * autrement
     */
    public DataReturn<String> modifyPassword(String email, String password) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        int result;
        if (conn != null) {
            try {
                String hashed = BCrypt.hashpw(password, BCrypt.gensalt(12));
                result = db.update(conn, SQLQuery.UPDATE_USER_PASSWORD_BY_EMAIL, hashed, email);
                if (result == 1) {
                    out.success("OK");
                } else {
                    out.fail("L'adresse ne semble pas être correcte", ErrorCode.INVALID_ENTRY);
                    out.setAffectedTables("t_user");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Ajoute un enregistrement dans la table de relation entre les professeurs
     * et les classes
     *
     * @param id l'id du professeur à qui ajouter la classe
     * @param uuid l'uuid de la classe
     * @return un DataReturn avec le String OK si tout se passe, le problème
     * autrement
     */
    public DataReturn<String> shareClasse(int id, String uuid) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        ResultSet count = null;
        ResultSet rs = null;
        if (conn != null) {
            try {
                count = db.select(conn, SQLQuery.COUNT_SHARES_FOR_CLASS, uuid);
                if (count != null && count.first() && count.getInt("sharesCount") < 5) {
                    rs = db.insert(conn, SQLQuery.INSERT_USER_CLASS, id, uuid);
                    if (rs != null) {
                        out.success("OK");
                    } else {
                        out.fail("Les éléments entrés ne semblent pas être correctes", ErrorCode.INVALID_ENTRY);
                        out.setAffectedTables("tr_user_class");
                    }
                } else {
                    out.fail("Cette classe ne peut plus être partagée", ErrorCode.TOO_MUCH_ENTRY);
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Récupère un exercice du degré indiqué pour le jeu indiqué
     *
     * @param id l'id du jeu
     * @param degre le degre de l'exercice
     * @return l'exercice
     */
    public DataReturn<Exercice> getExercice(String id, String degre) {
        Connection conn = db.open();
        DataReturn<Exercice> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_EXERCICE, degre, id);
                if (rs != null && rs.first()) {
                    Exercice ex = new Exercice();
                    ex.setExercice(rs.getString("exercice"));
                    ex.setId(rs.getInt("id"));
                    out.success(ex);
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Récupère un exercice forcé à travers son identifiant
     *
     * @param forcedExerciceId l'id de l'exercice à récupérer
     * @return l'exercice
     */
    public DataReturn<Exercice> getExerciceById(int forcedExerciceId) {
        Connection conn = db.open();
        DataReturn<Exercice> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_EXERCICE_BY_ID, forcedExerciceId);
                if (rs != null && rs.first()) {
                    Exercice ex = new Exercice();
                    ex.setExercice(rs.getString("json"));
                    ex.setId(rs.getInt("id"));
                    ex.setGameId(rs.getInt("game_id"));
                    ex.setGameName(rs.getString("game_name"));                    
                    ex.setDescription(rs.getString("description"));
                    out.success(ex);
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Récupère tous les exercices de jeux
     *
     * @return la liste d'exercices
     */
    public DataReturn<List<Exercice>> getAllExercices() {
        Connection conn = db.open();
        DataReturn<List<Exercice>> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_ALL_EXERCICES);
                if (rs != null && rs.first()) {
                    List<Exercice> exoList = new ArrayList<>();
                    do {
                        Exercice ex = new Exercice();
                        ex.setId(rs.getInt("id"));
                        ex.setExercice(rs.getString("json"));
                        ex.setGameId(rs.getInt("game_id"));
                        ex.setStateId(rs.getInt("state_id"));
                        ex.setDegreeId(rs.getInt("degree_id"));
                        String d = rs.getString("description");
                        if (d != null) {
                            ex.setDescription(d);
                        }
                        ex.setLevel(rs.getString("level"));
                        ex.setStudentId(rs.getInt("student_id"));
//                        FIXME : je mets en commentaire cette partie pour optimise le développement en local mais ce sera géré par le refactoring qui permettra d'afficher les images des exos lors du click sur le modal
//                        Blob b = rs.getBlob("image");
//                        // Certains exercices n'ont pas d'image
//                        if (b != null) {
//                            int length = (int) b.length();
//                            byte[] img = b.getBytes(1, length);
//                            // On récupère l'image déjà en format base64, donc pas besoin de la reconvertir
//                            ex.setImage(new String(img));
//                            System.out.println(ex.getImage());
//                        }
                        exoList.add(ex);
                    } while (rs.next());
                    out.success(exoList);
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Récupère tous les exercices d'un seul parcours à partir de leurs
     * ientifiants
     *
     * @param stepsId le tableau d'entiers avec les identifiants
     * @return l'exercice
     */
    public DataReturn<List<Exercice>> getParcoursExercices(String[] stepsId) {
        Connection conn = db.open();
        DataReturn<List<Exercice>> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                List<Exercice> exoList = new ArrayList<>();
                for (int i = 0; i < stepsId.length; i++) {
                    rs = db.select(conn, SQLQuery.GET_EXERCICE_BY_ID, stepsId[i]);
                    Exercice ex = new Exercice();
                    if (rs != null && rs.first()) {
                        System.out.println("les ids : " + stepsId[i]);
                        ex.setId(rs.getInt("id"));
                        ex.setExercice(rs.getString("exercice"));
                        ex.setGameId(rs.getInt("game_id"));
                        ex.setGameName(rs.getString("game_name"));
                        ex.setStateId(rs.getInt("stateId"));
//                        ex.setDegreeId(rs.getInt("degree_id"));
                        String d = rs.getString("description");
                        if (d != null) {
                            ex.setDescription(d);
                        }
                        ex.setLevel(rs.getString("niveau"));
                        Blob b = rs.getBlob("image");
                        // Certains exercices n'ont pas d'image
                        if (b != null) {
                            int length = (int) b.length();
                            byte[] img = b.getBytes(1, length);
                            // On récupère l'image déjà en format base64, donc pas besoin de la reconvertir
                            ex.setImage(new String(img));
                            System.out.println(ex.getImage());
                        }
                    }
                    exoList.add(ex);
                }
                out.success(exoList);
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Récupère tous les parcours partagés
     *
     * @return le parcours
     */
    public DataReturn<List<Parcours>> getAllSharedParcours() {
        Connection conn = db.open();
        DataReturn<List<Parcours>> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_ALL_SHARED_PARCOURS);
                if (rs != null && rs.first()) {
                    List<Parcours> publicParcoursList = new ArrayList<>();
                    do {
                        Parcours p = new Parcours();
                        p.setId(rs.getInt("user_id"));
                        p.setNom(rs.getString("name"));
                        p.setLicence(rs.getString("licence"));
                        p.setDegre(rs.getInt("degre_id"));
                        p.setDescription(rs.getString("description"));
                        p.setCompetences(rs.getString("competences"));
                        p.setSteps(rs.getString("steps"));
                        p.setCreated(rs.getString("created"));
                        p.setUpdated(rs.getString("updated"));
                        p.setOwner(rs.getString("ownerEmail"));
                        publicParcoursList.add(p);
                    } while (rs.next());
                    out.success(publicParcoursList);
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Récupère tous les parcours publics
     *
     * @return le parcours
     */
    public DataReturn<List<Parcours>> getAllPublicParcours() {
        Connection conn = db.open();
        DataReturn<List<Parcours>> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_ALL_PUBLIC_PARCOURS);
                if (rs != null && rs.first()) {
                    List<Parcours> publicParcoursList = new ArrayList<>();
                    do {
                        Parcours p = new Parcours();
                        p.setId(rs.getInt("user_id"));
                        p.setNom(rs.getString("name"));
                        p.setLicence(rs.getString("licence"));
                        p.setDegre(rs.getInt("degre"));
                        p.setDescription(rs.getString("description"));
                        p.setCompetences(rs.getString("competences"));
                        p.setSteps(rs.getString("steps"));
                        p.setCreated(rs.getString("created"));
                        p.setUpdated(rs.getString("updated"));
                        p.setOwner(rs.getString("ownerEmail"));
                        publicParcoursList.add(p);
                    } while (rs.next());
                    out.success(publicParcoursList);
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<String> getExerciceEleves(int degre, int id) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_EXERCICE_STUDENTS, degre, id);
                if (rs != null && rs.first()) {
                    JsonObject json = new JsonObject();
                    do {
                        JsonObject exerice = new JsonObject();
                        exerice.addProperty("name", " - "
                                + rs.getString("eleve") + " - "
                                + rs.getString("classe") + " - "
                                + "Harmos " + degre + " - " + new SimpleDateFormat("dd.MM.yyyy HH:mm").format(rs.getDate("date")));
                        exerice.add("exercice", new JsonParser().parse(rs.getString("exercice")));
                        json.add(rs.getInt("id") + "", exerice);
                    } while (rs.next());
                    out.success(json.toString());
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<String> getExericesForValidation() {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                JsonObject json = new JsonObject();
                JsonObject exercices = new JsonObject();
                JsonArray classes = new JsonArray();
                JsonArray games = new JsonArray();
                JsonArray degrees = new JsonArray();
                rs = db.select(conn, SQLQuery.GET_EXERCICE_FOR_VALIDATION);
                if (rs != null && rs.first()) {
                    do {
                        JsonObject exerice = new JsonObject();
                        exerice.addProperty("student", rs.getString("student"));
                        exerice.addProperty("classe", rs.getString("class"));
                        exerice.addProperty("degre", rs.getInt("degree"));
                        exerice.addProperty("date", new SimpleDateFormat("dd.MM.yyyy HH:mm").format(rs.getDate("date")));
                        exerice.addProperty("classeId", rs.getInt("class_id"));
                        exerice.addProperty("gameId", rs.getInt("game_id"));
                        exerice.addProperty("gameName", rs.getString("game_name"));
                        try {
                            exerice.add("exercice", new JsonParser().parse(rs.getString("exercice")));
                        } catch (JsonParseException e) {
                            // Gérer l'exception en cas de problème avec la syntaxe JSON
                        } catch (SQLException e) {
                            // Gérer l'exception en cas de problème avec la base de données
                        }
                        exercices.add(rs.getInt("pk") + "", exerice);
                    } while (rs.next());
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
                rs = db.select(conn, SQLQuery.GET_CLASSES);
                if (rs != null && rs.first()) {
                    do {
                        JsonObject classe = new JsonObject();
                        classe.addProperty("id", rs.getInt("id"));
                        classe.addProperty("nom", rs.getString("name"));
                        classes.add(classe);
                    } while (rs.next());
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
                rs = db.select(conn, SQLQuery.GET_GAMES_FOR_VALIDATION);
                if (rs != null && rs.first()) {
                    do {
                        JsonObject game = new JsonObject();
                        game.addProperty("id", rs.getInt("id"));
                        game.addProperty("name", rs.getString("name"));
                        games.add(game);
                    } while (rs.next());
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
                rs = db.select(conn, SQLQuery.GET_DEGREES);
                if (rs != null) {
                    while (rs.next()) {
                        JsonObject d = new JsonObject();
                        d.addProperty("nom", rs.getString("degree"));
                        d.addProperty("id", rs.getInt("id"));
                        degrees.add(d);
                    }
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
                json.add("exercices", exercices);
                json.add("degres", degrees);
                json.add("classes", classes);
                json.add("games", games);
                out.success(json.toString());
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<String> getParcoursBetaTracks() {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                JsonObject json = new JsonObject();
                JsonObject tracks = new JsonObject();
                rs = db.select(conn, SQLQuery.GET_PARCOURS_BETA_TRACKS);
                if (rs != null && rs.first()) {
                    int i = 1;
                    do {
                        JsonObject track = new JsonObject();
                        track.addProperty("studentName", rs.getString("student_name"));
                        track.addProperty("studentId", rs.getString("student_id"));
                        track.addProperty("exoId", rs.getString("exercice_id"));
                        track.addProperty("description", rs.getString("description"));
                        track.addProperty("parcoursName", rs.getString("parcours_name"));
                        track.addProperty("date", rs.getString("date"));
                        track.addProperty("startTime", rs.getString("start"));
                        track.addProperty("evaluation", rs.getString("evaluation"));
                        tracks.add("track" + i, track);
                        i++;
                    } while (rs.next());
                } else {
                    out.fail("Impossible de trouver une trace");
                }
                json.add("tracks", tracks);
                out.success(json.toString());
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<String> addExercice(String jsonExercice, int fk_jeu, int eleveID, int degre, Integer forcedState) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        int id = 0;
        if (conn != null) {
            try {
                ResultSet generatedKey = db.insert(conn, SQLQuery.INSERT_EXERCICE, jsonExercice, fk_jeu, eleveID, degre);
//                if (forcedState != null && forcedState > 0 && generatedKey.first()) {
//                    db.update(conn, SQLQuery.UPDATE_EXERCICE_STATE, forcedState, generatedKey.getInt(1));
//                }
//                out.success("OK");
                if (generatedKey.first()) {
                    id = generatedKey.getInt(1);
                }
                out.success(Integer.toString(id));
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<String> createUnpluggedActivity(String jsonExercice, int fk_jeu, int eleveID, String exoName) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        int id = 0;
        if (conn != null) {
            try {
                ResultSet generatedKey = db.insert(conn, SQLQuery.INSERT_UNPLUGGED_ACTIVITY, jsonExercice, fk_jeu, eleveID, exoName);
                if (generatedKey.first()) {
                    id = generatedKey.getInt(1);
                }
                out.success(Integer.toString(id));
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Met à jour le step dans la base de données (Modifie un exercice débranché
     * déjà existant)
     *
     * @param {int} exo_id l'id de l'exercice à mettre à jour
     * @param {string} content le contenu à assigner au step
     */
    public DataReturn<String> updateUnpluggedExercice(int exo_id, String exercice) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        int id = 0;
        int retour;
        if (conn != null) {
            try {
                retour = db.update(conn, SQLQuery.UPDATE_UNPLUGGED_EXERCICE, exercice, exo_id);
                if (retour == 1) {
                    out.success("ok");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

//    /**
//     * Récupère les informations essentielles pour une liste d'exercices (evaluation et status)   
//     * @param stepsId le String contenant les id des steps
//     * @return une liste des infos des exercices
//     */
//    public DataReturn<ArrayList<HistoriqueLine>> getStepsInfo(String stepsId) {
//        Connection conn = db.open();
//        DataReturn<ArrayList<HistoriqueLine>> out = new DataReturn<>();
//        String[] stepsIdarray = stepsId.split(",");
//        ArrayList<HistoriqueLine> stepsInfoList = new ArrayList<>();
//        ResultSet rs = null;
//        if (conn != null) {
//            try {
////                for (int i=0; i<stepsIdarray.length; i++){
////                    À chaque fois on envoie un seul id et on récupère les valeurs
////                    System.out.println(stepsIdarray[i]);
//                    rs = db.select(conn, SQLQuery.GET_EXERCICE_INFO_BY_ID);
//                    System.out.println(rs);
//                    if (rs != null && rs.first()) {
//                        do{
//                            System.out.println("hello");
//                            HistoriqueLine h = new HistoriqueLine();
//                            h.setEvaluation(rs.getString("evaluation"));
//                            h.setStatus(rs.getInt("status"));
//                            System.out.println(rs.getString("evaluation"));
//                            System.out.println(rs.getInt("status"));
//                            stepsInfoList.add(h);                            
//                            System.out.println(h.getEvaluation());
//                            System.out.println(h.getStatus());
//                            System.out.println(stepsInfoList);
//                        } while(rs.next());
//                    } else {                        
//                        out.fail("Impossible de trouver les statistiques");
//                    }
////                }
//                out.success(stepsInfoList);
//            } catch (SQLException ex) {
//                out.fail(ex.getMessage());
//            } finally {
//                db.close(conn);
//                try {
//                    if (rs != null && !rs.isClosed()) {
//                        rs.close();
//                    }                    
//                } catch (SQLException ex) {
//                    // rs déjà fermé
//                }
//            }
//        } else {
//            out.fail("Aucune connexion à la base de données n'a pu être établie!");
//        }
//        return out;
//    }
    /**
     * Récupère l'historique d'un élève pour un parcours donné
     *
     * @param studentId l'id de l'élève dont on va chercher les stats
     * @param parcoursId l'id du parcours en question
     * @return l'historique de l'élève
     */
    public DataReturn<HistoriqueEleve> getStudentParcoursStats(int studentId, int parcoursId) {
        Connection conn = db.open();
        DataReturn<HistoriqueEleve> out = new DataReturn<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        if (conn != null) {
            try {
                rs1 = db.select(conn, SQLQuery.GET_STATS_ELEVE_PARCOURS, studentId, parcoursId);
                if (rs1 != null) {
                    rs2 = db.select(conn, SQLQuery.GET_STUDENT_BY_ID, studentId);
                    if (rs2 != null && rs2.first()) {
                        HistoriqueEleve he = new HistoriqueEleve();
                        ArrayList<HistoriqueLine> historique = new ArrayList<>();
                        he.setNom_eleve(rs2.getString("name"));
                        he.setNom_classe(rs2.getString("class"));
                        while (rs1.next()) {
                            historique.add(fillHistoriqueLine(rs1));
                        }
                        he.setHistorique(historique);
                        out.success(he);
                    } else {
                        out.fail("L'élève n'existe pas");
                    }
                } else {
                    out.fail("Impossible de trouver les statistiques");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs1 != null && !rs1.isClosed()) {
                        rs1.close();
                    }
                    if (rs2 != null && !rs2.isClosed()) {
                        rs2.close();
                    }
                } catch (SQLException ex) {
                    // rs déjà fermé
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Récupère tout l'historique d'un élève
     *
     * @param id l'id de l'élève dont on va chercher les stats
     * @return l'historique de l'élève
     */
    public DataReturn<HistoriqueEleve> getStatsEleve(int id) {
        Connection conn = db.open();
        DataReturn<HistoriqueEleve> out = new DataReturn<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        if (conn != null) {
            try {
                rs1 = db.select(conn, SQLQuery.GET_STATS_ELEVE, id);
                if (rs1 != null) {
                    rs2 = db.select(conn, SQLQuery.GET_STUDENT_BY_ID, id);
                    if (rs2 != null && rs2.first()) {
                        HistoriqueEleve he = new HistoriqueEleve();
                        ArrayList<HistoriqueLine> historique = new ArrayList<>();
                        he.setNom_eleve(rs2.getString("name"));
                        he.setNom_classe(rs2.getString("class"));
                        while (rs1.next()) {
                            historique.add(fillHistoriqueLine(rs1));
                        }
                        he.setHistorique(historique);
                        out.success(he);
                    } else {
                        out.fail("L'élève n'existe pas");
                    }
                } else {
                    out.fail("Impossible de trouver les statistiques");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs1 != null && !rs1.isClosed()) {
                        rs1.close();
                    }
                    if (rs2 != null && !rs2.isClosed()) {
                        rs2.close();
                    }
                } catch (SQLException ex) {
                    // rs déjà fermé
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Récupère tout l'historique d'une classe
     *
     * @param id l'id de la classe dont on va chercher les stats
     * @return l'historique de la classe
     */
    public DataReturn<HistoriqueClasse> getStatsClasse(int id) {
        Connection conn = db.open();
        DataReturn<HistoriqueClasse> out = new DataReturn<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        if (conn != null) {
            try {
                rs1 = db.select(conn, SQLQuery.GET_STATS_CLASSE, id);
                if (rs1 != null) {
                    rs2 = db.select(conn, SQLQuery.GET_CLASS_BY_ID, id);
                    if (rs2 != null && rs2.first()) {
                        HistoriqueClasse hc = new HistoriqueClasse();
                        ArrayList<HistoriqueEleve> historique = new ArrayList<>();
                        hc.setNom_classe(rs2.getString("name"));
                        while (rs1.next()) {
                            historique.add(getStatsEleve(rs1.getInt("pk_eleve")).getData());
                        }
                        hc.setHistorique(historique);
                        out.success(hc);
                    } else {
                        out.fail("La classe n'existe pas");
                    }
                } else {
                    out.fail("Impossible de trouver les statistiques");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs1 != null && !rs1.isClosed()) {
                        rs1.close();
                    }
                    if (rs2 != null && !rs2.isClosed()) {
                        rs2.close();
                    }
                } catch (SQLException ex) {
                    // rs déjà fermé
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Crée une nouvelle statistique DAT
     *
     * @param studentId L'id de l'élève
     * @param exerciceId L'id de l'exercice en question
     * @param mode Le mode de jeu
     * @param parcoursId L'id du parcours en question si l'on est en mode
     * Parcours
     * @param datFunction La fonctionnalité additionnelle DAT
     * @return l'id de la stat créée si tout se passe bien, le problème
     * autrement
     */
    public DataReturn<Integer> newDATEntry(int studentId, int exerciceId, int mode, int parcoursId, String datFunction) {
        Connection conn = db.open();
        DataReturn<Integer> out = new DataReturn();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.insert(conn, SQLQuery.INSERT_DAT_STAT_START, studentId, exerciceId, mode, parcoursId, datFunction);
                if (rs != null && rs.first()) {
                    out.success(rs.getInt("id"));
                } else {
                    out.fail("Impossible d'ajouter les statistiques", ErrorCode.INVALID_ENTRY);
                    out.setAffectedTables("t_history_dat");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Requête async.pour ajouter une statistique
     *
     * @param mode Le mode de jeu
     * @param idJeu L'id du jeu
     * @param idEleve L'id de l'élève
     * @param exercice_id L'id de l'exercice en question
     * @param parcoursId L'id du parcours en question si l'on est en mode
     * Parcours
     * @return l'id de la stat créée si tout se passe bien, le problème
     * autrement
     */
    public DataReturn<Integer> addStats(int idEleve, int idJeu, int mode, int exercice_id, int parcoursId) {
        Connection conn = db.open();
        DataReturn<Integer> out = new DataReturn();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.insert(conn, SQLQuery.INSERT_STAT_START, idEleve, idJeu, mode, exercice_id, parcoursId);
                if (rs != null && rs.first()) {
                    out.success(rs.getInt("id"));
                    out.success(rs.getInt("student_id"));
                    out.success(rs.getInt("parcours_id"));
                    out.success(rs.getInt("exercice_id"));
                } else {
                    out.fail("Impossible d'ajouter les statistiques", ErrorCode.INVALID_ENTRY);
                    out.setAffectedTables("t_history");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<String> updateDATEntry(int id, String content) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn();
        int retour;
        if (conn != null) {
            try {
                retour = db.update(conn, SQLQuery.UPDATE_DAT_STAT_END, content, id);
                if (retour == 1) {
                    out.success("ok");
                } else {
                    out.fail("Impossible de modifier les statistiques", ErrorCode.INVALID_ENTRY);
                    out.setAffectedTables("t_history_dat");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!" + db.getErrDB());
        }
        return out;
    }

    public DataReturn<String> updateStudentCollection(int studentId, String collectionData) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn();
        int retour;
        if (conn != null) {
            try {
                retour = db.update(conn, SQLQuery.UPDATE_STUDENT_COLLECTION, collectionData, studentId);
                if (retour == 1) {
                    out.success("ok");
                } else {
                    out.fail("Impossible de modifier la collection", ErrorCode.INVALID_ENTRY);                    
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!" + db.getErrDB());
        }
        return out;
    }

    public DataReturn<String> updateStats(int id, String evaluation, String details) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn();
        int retour;
        if (conn != null) {
            try {
                retour = db.update(conn, SQLQuery.UPDATE_STAT_START, evaluation, details, id);
                if (retour == 1) {
                    out.success("ok");
                } else {
                    out.fail("Impossible de modifier les statistiques", ErrorCode.INVALID_ENTRY);
                    out.setAffectedTables("t_history");
                }
            } catch (SQLException | NullPointerException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!" + db.getErrDB());
        }
        return out;
    }

    private HistoriqueLine fillHistoriqueLine(ResultSet rs) throws SQLException {
        HistoriqueLine hl = new HistoriqueLine();
        hl.setPk_jeu(rs.getInt("pk_jeu"));
        hl.setNom_jeu(rs.getString("nom_jeu"));
        hl.setPk_discipline(rs.getInt("pk_discipline"));
        hl.setNom_discipline(rs.getString("nom_discipline"));
        hl.setHarmos(rs.getInt("fk_degre"));
        Calendar cal = Calendar.getInstance();
        Date date = rs.getDate("date");
        cal.setTime(date);
        hl.setDate(cal.getTime());
        Date start = rs.getDate("start");
        cal.setTime(start);
        hl.setStart(cal.getTime());
        Date end = rs.getDate("end");
        cal.setTime(end);
        hl.setEnd(cal.getTime());
        hl.setEvaluation(rs.getInt("evaluation"));
        hl.setStatus(rs.getInt("status"));
        hl.setExerciceId(rs.getInt("exercice_id"));
        hl.setParcoursId(rs.getInt("parcours_id"));
        return hl;
    }

    /**
     * Retourne la dernière erreur de la DB
     *
     * @return la dernière erreur
     */
    public String getErrDB() {
        return errDB;
    }

    public DataReturn<List<User>> getUsers() {
        Connection conn = db.open();
        DataReturn<List<User>> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_USERS);
                if (rs != null && rs.first()) {
                    List<User> userList = new ArrayList<>();
                    do {
                        User u = new User();
                        u.setRole(rs.getString("role"));
                        u.setEmail(rs.getString("email"));
                        u.setRoleId(rs.getInt("roleId"));
                        u.setUserId(rs.getInt("userId"));
                        userList.add(u);
                    } while (rs.next());
                    out.success(userList);
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Requête async. pour récupérer la collection d'un élève
     *
     * @param studentId L'id de l'élève
     * @return la collection de l'élève si tout se passe bien, le problème
     * autrement
     */
    public DataReturn<String> getStudentCollection(int studentId) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_STUDENT_COLLECTION, studentId);
                if (rs != null && rs.first()) {
                    out.success(rs.getString("collection"));
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<User> updateRole(int userId, int role) {
        Connection conn = db.open();
        DataReturn<User> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                db.update(conn, SQLQuery.UPDATE_ROLE, role, userId);
                rs = db.select(conn, SQLQuery.GET_USER_FROM_ID, userId);
                if (rs != null && rs.first()) {
                    User u = new User();
                    u.setRole(rs.getString("role"));
                    u.setEmail(rs.getString("email"));
                    u.setRoleId(rs.getInt("roleId"));
                    u.setUserId(rs.getInt("userId"));
                    out.success(u);
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public boolean checkPasswordForId(int profId, String password) {
        Connection conn = db.open();
        ResultSet rs = null;
        if (conn != null) {
            try {
                rs = db.select(conn, SQLQuery.GET_USER_BY_ID, profId);
                if (rs != null && rs.first()) {
                    if (BCrypt.checkpw(password, rs.getString("password"))) {
                        return true;
                    }
                }
            } catch (SQLException | NullPointerException ex) {
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                }
            }
        }
        return false;
    }

    public DataReturn<String> signaler(String message, int exId, int profId) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        if (conn != null) {
            try {
                db.insert(conn, SQLQuery.INSERT_REPORTING, message, profId, exId);
                db.update(conn, SQLQuery.UPDATE_EXERCICE_STATE, 3, exId);
                out.success("OK");
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<String> getExerciceSignale() {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                JsonObject exercices = new JsonObject();
                rs = db.select(conn, SQLQuery.GET_REPORTING);
                if (rs != null && rs.first()) {
                    do {
                        JsonObject exerice = new JsonObject();
                        exerice.addProperty("name", " - "
                                + rs.getString("student") + " - "
                                + rs.getString("class") + " - "
                                + rs.getString("degree") + " - " + new SimpleDateFormat("dd.MM.yyyy HH:mm").format(rs.getDate("date")));
                        exerice.addProperty("professeur", rs.getString("user"));
                        exerice.addProperty("message", rs.getString("message"));
                        exerice.addProperty("signalementId", rs.getInt("reportingId"));
                        exerice.addProperty("gamesId", rs.getInt("gameId"));
                        exerice.add("exercice", new JsonParser().parse(rs.getString("exercice")));
                        exercices.add(rs.getInt("exerciceId") + "", exerice);
                    } while (rs.next());
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
                out.success(exercices.toString());
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /*
    ** Obtenir une vue complète d'un exercice, incluant les détails de l'élève, de la classe et du scénario.
     */
    public DataReturn<String> getExericesById(int exId) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        ResultSet rs = null;
        if (conn != null) {
            try {
                JsonObject exercice = new JsonObject();
                rs = db.select(conn, SQLQuery.GET_EXERCICES_BY_ID, exId);
                if (rs != null && rs.first()) {
                    do {
                        exercice.addProperty("name", " - "
                                + rs.getString("eleve") + " - "
                                + rs.getString("classe") + " - "
                                + "Harmos " + rs.getInt("degree") + " - " + new SimpleDateFormat("dd.MM.yyyy HH:mm").format(rs.getDate("date")));
                        exercice.addProperty("degre", rs.getInt("degree"));
                        exercice.addProperty("classe", rs.getInt("classeId"));
                        exercice.add("exercice", new JsonParser().parse(rs.getString("exercice")));
                        if (rs.getString("scenario") != null) {
                            exercice.add("scenario", new JsonParser().parse(rs.getString("scenario")));
                        }
                    } while (rs.next());
                } else {
                    out.fail("Impossible de trouver un exercice");
                }
                out.success(exercice.toString());
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
                try {
                    if (rs != null && !rs.isClosed()) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    // rs deja ferme
                }
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<String> updateExercice(String exercice, String level, int exId) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        if (conn != null) {
            try {
                db.update(conn, SQLQuery.UPDATE_EXERCICE, exercice, level, exId);
                out.success("OK");
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<String> updateExerciceState(String description, int state, String level, String imageB64, String metaDataJson, int exId) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        if (conn != null) {
            try {
                db.update(conn, SQLQuery.UPDATE_EXERCICE_STATE, description, state, level, imageB64, metaDataJson, exId);
                out.success("OK");
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<HashMap> getStatsByEleveForGrapic(int eleveId) {
        Connection conn = db.open();
        DataReturn<HashMap> out = new DataReturn<>();
        if (conn != null) {
            HashMap<String, Object> data = new HashMap<>();
            try {
                ResultSet rs = db.select(conn, SQLQuery.GET_STATS_BY_ELEVE_FOR_GRAPHIC, eleveId);
                TreeMap<Date, HashMap<Integer, Statistiques>> statsMap = new TreeMap<>();
                while (rs.next()) {
                    Date d = rs.getDate("date");
                    if (statsMap.get(d) == null) {
                        statsMap.put(d, new HashMap<>());
                    }
                    Integer jeu = rs.getInt("jeu");
                    Statistiques stats = new Statistiques();
                    stats.setJeu(rs.getString("nomJeu"));
                    stats.setPartieCommence(rs.getInt("partieCommence"));
                    stats.setPartieTermine(rs.getInt("partieTermine"));
                    stats.setScore0(rs.getInt("count0"));
                    stats.setScore1(rs.getInt("count1"));
                    stats.setScore2(rs.getInt("count2"));

                    ResultSet rsTag = db.select(conn, SQLQuery.GET_TAGS_BY_GAME, jeu);
                    while (rsTag.next()) {
                        stats.addTag(rsTag.getInt("id"));
                    }
                    statsMap.get(d).put(jeu, stats);
                }
                rs = db.select(conn, SQLQuery.GET_TAGS_AND_GAME);
                HashMap<Integer, Tag> tagsMap = new HashMap<>();
                while (rs.next()) {
                    int id = rs.getInt("tag_id");
                    Tag tag = tagsMap.get(id);
                    if (tag == null) {
                        tag = new Tag(id, rs.getString("tag"));
                        tag.addJeu(rs.getString("game"));
                        tagsMap.put(id, tag);
                    } else {
                        tag.addJeu(rs.getString("game"));
                    }
                }
                rs = db.select(conn, SQLQuery.GET_GAMES);
                HashMap<Integer, GamesForStats> gamesMap = new HashMap<>();
                while (rs.next()) {
                    GamesForStats game = new GamesForStats();
                    game.setName(rs.getString("name"));
                    game.setEvaluation(rs.getString("evaluation"));
                    gamesMap.put(rs.getInt("id"), game);
                }
                data.put("stats", statsMap);
                data.put("tags", tagsMap.values());
                data.put("games", gamesMap);
                out.success(data);
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<String> deleteExercice(int exId) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        if (conn != null) {
            try {
                db.delete(conn, SQLQuery.DELETE_REPORTING_BY_EXERCICE_ID, exId);
                db.delete(conn, SQLQuery.DELETE_EXERCICE_BY_ID, exId);
                out.success("OK");
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn<String> deleteParcoursFrom(String element, int target_id, int parcours_id) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        if (conn != null) {
            try {
                if (element.equals("class")) {
                    db.delete(conn, SQLQuery.DELETE_PARCOURS_FROM_CLASS, target_id, parcours_id);
                } else if (element.equals("student")) {
                    db.delete(conn, SQLQuery.DELETE_PARCOURS_FROM_STUDENT, target_id, parcours_id);
                }
                out.success("OK");
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Supprime un parcours à travers son Id
     *
     * @return la dernière erreur
     */
    public DataReturn<String> deleteParcoursWithId(int parcours_id) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        if (conn != null) {
            try {
                // On supprime déjà le parcours depuis les tables de relations pour éviter les pb de clé étrangères
                db.delete(conn, SQLQuery.DELETE_PARCOURS_FROM_CLASS_WITHOUT_CLASS_ID, parcours_id);
                db.delete(conn, SQLQuery.DELETE_PARCOURS_FROM_STUDENT_WITHOUT_STUDENT_ID, parcours_id);
                // On supprime ensuite depuis la table t_parcours
                db.delete(conn, SQLQuery.DELETE_PARCOURS_WITH_ID, parcours_id);
                out.success("OK");
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    /**
     * Réinitialise un parcours pour un élève en supprimant les historiques de
     * ses exercices dans le parcours
     *
     * @param studentId l'id de l'élève
     * @param parcoursId l'id du parcours
     * @return un DataReturn avec le String OK si tout se passe bien, le
     * problème sinon
     *
     */
    public DataReturn<String> resetParcoursForStudent(int studentId, int parcoursId) {
        Connection conn = db.open();
        DataReturn<String> out = new DataReturn<>();
        if (conn != null) {
            try {
                db.delete(conn, SQLQuery.DELETE_HISTORY_BY_EXERCICE_AND_PARCOURS_ID, studentId, parcoursId);
                out.success("OK");
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }

    public DataReturn getSprites(int id) {
        Connection conn = db.open();
        DataReturn<ArrayList> out = new DataReturn<>();
        if (conn != null) {
            try {
                ArrayList<Sprite> lstSprites = new ArrayList<>();
                ResultSet rsSprites = db.select(conn, SQLQuery.GET_SPRITES_BY_GAME, id);
                while (rsSprites != null && rsSprites.next()) {
                    Sprite spr = new Sprite();
                    spr.setNom(rsSprites.getString("name"));
                    spr.setLongueur(rsSprites.getInt("width"));
                    spr.setHauteur(rsSprites.getInt("height"));
                    spr.setFrames(rsSprites.getInt("frames"));
                    Blob b = rsSprites.getBlob("image");
                    int length = (int) b.length();
                    byte[] img = b.getBytes(1, length);
                    byte[] base64 = Base64.encodeBase64(img);
                    spr.setBase64(new String(base64));
                    lstSprites.add(spr);
                };
                out.success(lstSprites);
            } catch (SQLException ex) {
                out.fail(ex.getMessage());
            } finally {
                db.close(conn);
            }
        } else {
            out.fail("Aucune connexion à la base de données n'a pu être établie!");
        }
        return out;
    }
}
