/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.hep3.srv.rest;

import ch.emfinfopro.hep3.srv.beans.Salt;
import ch.emfinfopro.netviewer.lib.beans.DataReturn;
import ch.emfinfopro.netviewer.lib.beans.DataReturn.ReturnType;
import ch.emfinfopro.netviewer.lib.beans.Info;
import ch.emfinfopro.hep3.srv.wrk.Wrk;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Servic
 *
 * @author carre
 */
@Path("rest")
public class REST {

    private static String invalidSaltResponse;
    private Wrk db;

    private final static String VERSION = "V0.7";
    private final static String EMAIL = "kollyf01@studentfr.ch";
    private boolean debug = true;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of REST
     */
    public REST() {
        invalidSaltResponse = (new DataReturn<>(ReturnType.SALT, "Invalid salt")).toString();
        db = new Wrk();
        this.debug = true;
    }

    /**
     * Obtient les informations du serveur.
     *
     * @return La version de l'application serveur et l'e-mail du développeur de
     * la version.
     */
    @GET
    @Path("/info")
    @Produces(MediaType.APPLICATION_JSON)
    public String info() {
        return new DataReturn<>(DataReturn.ReturnType.SUCCESS, new Info(VERSION, EMAIL)).toString();
    }

    /**
     * Vérifie l'email et le mot de passe d'un professeur
     *
     * @param email l'email du professeur
     * @param password le mot de passe du compte
     * @param salt le salt de communication
     * @return le professeur si tout va bien, le problème autrement
     */
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String checkLogin(@FormParam("email") String email, @FormParam("password") String password, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.checkLogin(email, password).toString();
    }

    /**
     * Récupère les données d'un professeur à partir de son Id
     *
     * @param id l'id du professeur
     * @param salt le salt de communication
     * @return le professeur, l'élément qui ne va pas sinon.
     */
    @POST
    @Path("/getTeacherById")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTeacherById(@FormParam("id") int id, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.getTeacherById(id).toString();
    }

    /**
     * Crée un nouveau compte pour un professeur
     *
     * @param email l'email du professeur
     * @param password le mot de passe du compte
     * @param code le code de confirmation
     * @param salt le salt de communication
     * @return le nom d'utilisateur si le compte a bien été créé, le problème
     * autrement
     */
    @POST
    @Path("/signup")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String createUser(
            @FormParam("email") String email,
            @FormParam("password") String password,
            @FormParam("code") String code, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.createUser(email, password, code).toString();
    }

    /**
     * Confirme un utilisateur via son code
     *
     * @param code le code de l'utilisateur à confirmer
     * @param salt le salt de communication
     * @return 1 si tout se passe bien, le problème autrement
     */
    @POST
    @Path("/confirm")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String confirmUser(@FormParam("id") String code, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.confirmUser(code).toString();
    }

    /**
     * Obtient les classes d'un professeur
     *
     * @param id la PK du professeur
     * @param salt le salt de communication
     * @return la liste des classes du professeur si tout va bien, le problème
     * autrement
     */
    @POST
    @Path("/getClasses")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String getClasses(@FormParam("id") int id, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }

        String dbReturn = "";

        try {
            dbReturn = db.getClasses(id).toString();
        } catch (Exception e) {
            dbReturn = e.getMessage();
        } finally {
            if (debug) {
                db.addLog("RESTful", "getClasses", dbReturn);
            }
        }

        return dbReturn;
    }

    /**
     * Obtient la classe via sa PK
     *
     * @param id la PK de la classe
     * @param salt le salt de communication
     * @return la classe possédant la PK indiquée
     */
    @POST
    @Path("/getClasse")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String getClasse(@FormParam("id") Integer id, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }

        if (debug) {
            String nameofCurrMethod = new Throwable()
                    .getStackTrace()[0]
                    .getMethodName();
            db.addLog("RESTful", nameofCurrMethod, "Salt: " + salt + " | ");
        }

        return db.getClasse(id).toString();
    }

    /**
     * Obtient la liste des élèves d'un professeur
     *
     * @param id la PK du professeur
     * @param salt le salt de communication
     * @return la liste des élèves qui se trouvent dans une des classes du
     * professeur si tout va bien, le problème autrement
     */
    @POST
    @Path("/getEleves")
    @Produces(MediaType.APPLICATION_JSON)
    public String getEleves(@FormParam("id") int id, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }

        String dbReturn = "";

        try {
            dbReturn = db.getEleves(id).toString();
        } catch (Exception e) {
            dbReturn = e.getMessage();
        } finally {
            if (debug) {
                db.addLog("RESTful", "getEleves", dbReturn);
            }
        }

        return dbReturn;
    }

    /**
     * Obtient la liste des parcours d'un professeur
     *
     * @param user_id la PK du professeur
     * @param salt le salt de communication
     * @return la liste des parcours qui correspondent à l'id du professeur
     */
    @POST
    @Path("/getParcours")
    @Produces(MediaType.APPLICATION_JSON)
    public String getParcours(@FormParam("id") int user_id, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }

        String dbReturn = "";

        try {
            dbReturn = db.getParcours(user_id).toString();
        } catch (Exception e) {
            dbReturn = e.getMessage();
        } finally {
            if (debug) {
                db.addLog("RESTful", "getParcours", dbReturn);
            }
        }

        return dbReturn;
    }

    /**
     * Récupère tous les exercices d'un seul parcours à partir de leurs
     * identifiants
     *
     * @param stepsId le tableau d'entiers avec les identifiants
     * @param salt le salt de communication
     * @return l'exercice
     */
    @POST
    @Path("/getParcoursExercices")
    @Produces(MediaType.APPLICATION_JSON)
    public String getParcoursExercices(@FormParam("stepsId") String[] stepsId, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        String dbReturn = "";
        try {
            dbReturn = db.getParcoursExercices(stepsId).toString();
        } catch (Exception e) {
            dbReturn = e.getMessage();
        } finally {
            if (debug) {
                db.addLog("RESTful", "getParcours", dbReturn);
            }
        }
        return dbReturn;
    }

    /**
     * Obtient la liste des parcours d'un professeur pour un(e) classe/élève
     * donné(e)
     *
     * @param target l'élément ciblé : classe ou élève
     * @param target_id l'id de la classe/élève recherché
     * @return la liste des élèves qui se trouvent dans une des classes du
     * professeur si tout va bien, le problème autrement
     */
    @POST
    @Path("/getAllParcoursOfTarget")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllParcoursOfTarget(@FormParam("target") String target, @FormParam("targetId") int target_id) {

        String dbReturn = "";

        try {
            dbReturn = db.getAllParcoursOfTarget(target, target_id).toString();
        } catch (Exception e) {
            dbReturn = e.getMessage();
        } finally {
            if (debug) {
                db.addLog("RESTful", "getAllParcoursOfTarget", dbReturn);
            }
        }

        return dbReturn;
    }

    /**
     * Obtient l'élève via sa PK
     *
     * @param id la PK de l'élève
     * @param salt le salt de communication
     * @return l'élève si tout se passe bien, le problème autrement
     */
    @POST
    @Path("/getEleve")
    @Produces(MediaType.APPLICATION_JSON)
    public String getEleve(@FormParam("id") int id, @FormParam("salt") String salt) {
        String dbReturn = "";

        try {
            if (!Salt.compare(salt)) {
                return invalidSaltResponse;
            }

            dbReturn = db.getEleve(id).toString();

        } catch (Exception e) {
            dbReturn = e.getMessage();
        } finally {
            if (debug) {
                db.addLog("RESTful", "getEleve", dbReturn);
            }
        }

        return dbReturn;
    }

    /**
     * Ajoute une classe pour un professeur et des élèves dans cette classe
     *
     * @param nom le nom de la classe
     * @param degre le degrès Harmos de la classe
     * @param id la PK du professeur
     * @param eleves le String contenant les noms des élèves à ajouter dans la
     * classe, séparés par un %
     * @param salt le salt de communication
     * @return la classe ajoutée si tout se passe bien, le problème autrement
     */
    @POST
    @Path("/addClasse")
    @Produces(MediaType.APPLICATION_JSON)
    public String addClasse(@FormParam("nom") String nom, @FormParam("degre") int degre, @FormParam("id") int id, @FormParam("eleves") List<String> eleves, @FormParam("salt") String salt) {

        String infoRetour = "";
        if (!Salt.compare(salt)) {
            infoRetour = invalidSaltResponse;
        } else {
            infoRetour = db.addClasse(nom, degre, id, eleves).toString();
        }

        db.addLog("RESTful", "addClasse", "Nom: " + nom + " | " + "Degre: " + degre + " | " + "Id: " + id + " | " + "Eleves: " + eleves + " | " + "Salt: " + salt + " | " + "InfoRetour: " + infoRetour);

        return infoRetour;
    }

    /**
     * Ajoute un élève dans une classe
     *
     * @param nom le nom de l'élève
     * @param classe la PK de la classe dans laqelle ajouter l'élève
     * @param salt le salt de communication
     * @return l'élève si tout se passe bien, le problème autrement
     */
    @POST
    @Path("/addEleve")
    @Produces(MediaType.APPLICATION_JSON)
    public String addEleve(@FormParam("nom") String nom, @FormParam("classe") int classe, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        if (debug) {
            db.addLog("RESTful", "addEleve", "Nom: " + nom + " | " + "Classe: " + String.valueOf(classe) + " | " + "Salt: " + salt);
        }

        return db.addEleve(nom, classe).toString();
    }

    /**
     * Ajoute un nouveau parcours
     *
     * @param user_id l'id de l'utilisateur (teacherId)
     * @param nom le nom du parcours
     * @param targetSkills les compétences cibles choisies pour ce parcours
     * @param salt le salt de communication
     * @return l'élève si tout se passe bien, le problème autrement
     */
    @POST
    @Path("/createParcours")
    @Produces(MediaType.APPLICATION_JSON)
    public String createParcours(@FormParam("user_id") int user_id, @FormParam("nom") String nom, @FormParam("targetSkills") String targetSkills, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        if (debug) {
            //db.addLog("RESTful", "addParcours", "Nom: " + nom + " | " + "Classe: " + String.valueOf(classe) + " | " + "Salt: " + salt);
            db.addLog("RESTful", "saveParcours", "Nom: ");
        }
        return db.createParcours(user_id, nom, targetSkills).toString();
    }

    /**
     * Ajoute un nouveau parcours
     *
     * @param user_id l'id de l'utilisateur (teacherId) : compose la PK
     * @param nom le nom du parcours : compose la PK
     * @param licence la licence
     * @param description la description
     * @param degre le degre Harmos du parcours
     * @param competences les competences
     * @param steps les étapes du parcours (en un seul string)
     * @param salt le salt de communication
     * @return l'élève si tout se passe bien, le problème autrement
     */
    @POST
    @Path("/saveParcours")
    @Produces(MediaType.APPLICATION_JSON)
    public String saveParcours(@FormParam("user_id") int user_id, @FormParam("nom") String nom, @FormParam("licence") String licence, @FormParam("description") String description, @FormParam("degre") int degre, @FormParam("competences") String competences, @FormParam("steps") String steps, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        if (debug) {
            //db.addLog("RESTful", "addParcours", "Nom: " + nom + " | " + "Classe: " + String.valueOf(classe) + " | " + "Salt: " + salt);
            db.addLog("RESTful", "saveParcours", "Nom: ");
        }
        return db.saveParcours(user_id, nom, licence, description, degre, competences, steps).toString();
    }

    /**
     * Assigner un parcours à une classe ou à un élève
     *
     * @param target pour savoir si on fait la requete sur une classe ou sur un
     * élève
     * @param target_id l'id de la classe ou l'èlève qui va recevoir le parcours
     * @param parcours_id l'id du parcours
     * @param initialSteps les étapes initiales du parcours
     * @param salt le salt de communication
     * @return le parcours si tout se passe bien, le problème autrement
     */
    @POST
    @Path("/saveParcoursTo")
    @Produces(MediaType.APPLICATION_JSON)
    public String saveParcoursTo(@FormParam("target") String target, @FormParam("targetId") int target_id, @FormParam("parcoursId") int parcours_id, @FormParam("initialSteps") String initialSteps, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        if (debug) {
            //db.addLog("RESTful", "addParcours", "Nom: " + nom + " | " + "Classe: " + String.valueOf(classe) + " | " + "Salt: " + salt);
            db.addLog("RESTful", "saveParcoursTo", "Nom: ");
        }
        return db.saveParcoursTo(target, target_id, parcours_id, initialSteps).toString();
    }

    /**
     * Met à jour les MetaData d'un parcours
     *
     * @param title Le nom du parcours
     * @param description La description du parcours
     * @param licence La licence utilisée pour créer le parcours
     * @param degre Le degre Harmos du parcours
     * @param tag Le(s) tag(s) associés au parcours
     * @param parcoursId L'id du parcours en question
     * @param salt le salt de communication
     */
    @POST
    @Path("/updateParcoursMetaData")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateParcoursMetaData(
            @FormParam("title") String title,
            @FormParam("description") String description,
            @FormParam("licence") String licence,
            @FormParam("degre") String degre,
            @FormParam("tag") String tag,
            @FormParam("parcoursId") int parcoursId,
            @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        if (debug) {
            db.addLog("RESTful", "updateParcoursMetData", "parcours: " + title);
        }
        return db.updateParcoursMetaData(title, description, licence, degre, tag, parcoursId).toString();
    }

    /**
     * Modifie le statut d'un parcours (privé / en attente / public)
     *
     * @param id l'id du parcours
     * @param status la valeur du statut : 0=privé, 1=en attente de publication,
     * 2=public
     * @param salt le salt de communication
     * @return le parcours si tout se passe bien, le problème autrement
     */
    @POST
    @Path("/updateParcoursStatus")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateParcoursStatus(@FormParam("id") int id, @FormParam("status") int status, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        if (debug) {
            db.addLog("RESTful", "updateParcours", "parcours_id: " + id + " parcours_statut : " + status);
        }
        return db.updateParcoursStatus(id, status).toString();
    }

    /**
     * Met à jour l'état de l'étape dans le json
     *
     * @param steps_json le json des étapes du parcours
     * @param parcours_id l'id du parcours à mettre à jour
     * @param salt le salt de communication
     * @return un message de succès, le problème autrement
     */
    @POST
    @Path("/updateStepsJson")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateStepsJson(@FormParam("parcoursId") int parcours_id, @FormParam("stepsJson") String steps_json, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.updateStepsJson(parcours_id, steps_json).toString();
    }
    
     /**
     * Met à jour les étapes d'un parcours personnalisé (adpative learning)
     *
     * @param student_id l'id de l'élève
     * @param custom_parcours_id l'id du custom_parcours à mettre à jour
     * @param custom_steps_json le json des étapes du parcours
     * @return un message de succès, le problème autrement
     */
    @POST
    @Path("/updateCustomStepsJson")
    @Produces(MediaType.APPLICATION_JSON)
    
    public String updateCustomStepsJson(@FormParam("studentId") int student_id, @FormParam("customParcoursId") int custom_parcours_id, @FormParam("customStepsJson") String custom_steps_json, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.updateCustomStepsJson(student_id, custom_parcours_id, custom_steps_json).toString();
    }

    /**
     * Met à jour un parcours existant
     *
     * @param user_id l'id de l'utilisateur (teacherId) : compose la PK
     * @param nom le nom du parcours : compose la PK
     * @param licence la licence
     * @param description la description
     * @param competences les competences
     * @param steps les étapes du parcours (en un seul string)
     * @param salt le salt de communication
     * @return l'élève si tout se passe bien, le problème autrement
     */
    @POST
    @Path("/updateParcours")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateParcours(@FormParam("user_id") int user_id, @FormParam("nom") String nom, @FormParam("licence") String licence, @FormParam("description") String description, @FormParam("competences") String competences, @FormParam("steps") String steps, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        if (debug) {
            //db.addLog("RESTful", "addParcours", "Nom: " + nom + " | " + "Classe: " + String.valueOf(classe) + " | " + "Salt: " + salt);
            db.addLog("RESTful", "updateParcours", "Nom: ");
        }
        return db.updateParcours(user_id, nom, licence, description, steps).toString();
    }

    /**
     * Obtient la liste des degrés Harmos
     *
     * @return la liste des degrés Harmos si tout se passe bien, le problème
     * autrement
     */
    @GET
    @Path("/getDegres")
    @Produces(MediaType.APPLICATION_JSON)
    public String getDegres() {
        return db.getDegres().toString();
    }

    @GET
    @Path("/getParcoursBetaTracks")
    @Produces(MediaType.APPLICATION_JSON)
    public String getParcoursBetaTracks() {
        return db.getParcoursBetaTracks().toString();
    }

    /**
     * Obtient la liste des disciplines
     *
     * @return la liste des disciplines si tout se passe bien, le problème
     * autrement
     */
    @GET
    @Path("/getDisciplines")
    @Produces(MediaType.APPLICATION_JSON)
    public String getDisciplines() {
        return db.getDisciplines().toString();
    }

    /**
     * Obtient la liste des langues
     *
     * @return la liste des langues si tout se passe bien, le problème autrement
     */
    @GET
    @Path("/getLangues")
    @Produces(MediaType.APPLICATION_JSON)
    public String getLangues() {
        return db.getLangues().toString();
    }

    /**
     * Obtient la liste des emails
     *
     * @return la liste des emails si tout se passe bien, le problème autrement
     */
    @GET
    @Path("/getEmails")
    @Produces(MediaType.APPLICATION_JSON)
    public String getEmails() {
        return db.getEmails().toString();
    }

    /**
     * Obtient la liste des tags
     *
     * @return la liste des tags si tout se passe bien, le problème autrement
     */
    @GET
    @Path("/getTags")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTags() {
        return db.getTags().toString();
    }

    /**
     * Obtient la liste des jeux
     *
     * @return la liste des jeux si tout se passe bien, le problème autrement
     */
    @GET
    @Path("/getJeux")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJeux() {
        return db.getJeux().toString();
    }

    /**
     * Obtient le jeu via sa PK
     *
     * @param id la PK du jeu à récupérer
     * @return le jeu souhaité si tout se passe bien, le problème aurement
     */
    @POST
    @Path("/getJeu")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJeu(@FormParam("id") int id) {
        return db.getJeu(id).toString();
    }

    /**
     * Modifie le nom d'un élève
     *
     * @param id la Pk de l'élève à modifier
     * @param name le nouveau nom
     * @param salt le salt de communication
     * @return l'élève avec son nouveau nom
     */
    @PUT
    @Path("/updateEleve")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateEleve(@FormParam("id") int id, @FormParam("name") String name, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.updateEleve(id, name).toString();
    }

    /**
     * Ajoute un code de récupération au professeur avec l'email indiqué
     *
     * @param email l'email du professeur à qui ajouter le code
     * @param code le code à ajouter
     * @param salt le salt de communication
     * @return un DataReturn avec le String OK si tout se passe, le problème
     * autrement
     */
    @PUT
    @Path("/addCodeToProfesseur")
    @Produces(MediaType.APPLICATION_JSON)
    public String addCodeToProfesseur(@FormParam("email") String email, @FormParam("code") String code, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.addCodeToProfesseur(email, code).toString();
    }

    /**
     * Vérifie le code de récupération pour un professeur
     *
     * @param code le code entré par le professeur
     * @param email l'email du professeur entrant le code
     * @param salt le salt de communication
     * @return un DataReturn avec le String OK si tout se passe, le problème
     * autrement
     */
    @POST
    @Path("/verifyCodeForRecovery")
    @Produces(MediaType.APPLICATION_JSON)
    public String verifyCodeForRecovery(@FormParam("code") String code, @FormParam("email") String email, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.verifyCodeForRecovery(code, email).toString();
    }

    /**
     * Modifie le mot de passe du professeur avec l'email indiqué
     *
     * @param email l'email du professeur
     * @param password le nouveau mot de passe
     * @param salt le salt de communication
     * @return un DataReturn avec le String OK si tout se passe, le problème
     * autrement
     */
    @PUT
    @Path("/modifyPassword")
    @Produces(MediaType.APPLICATION_JSON)
    public String modifyPassword(@FormParam("email") String email, @FormParam("password") String password, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.modifyPassword(email, password).toString();
    }

    /**
     * Ajoute un enregistrement dans la table de relation entre les professeurs
     * et les classes
     *
     * @param id l'id du professeur à qui ajouter la classe
     * @param uuid l'uuid de la classe
     * @param salt le salt de communication
     * @return un DataReturn avec le String OK si tout se passe, le problème
     * autrement
     */
    @PUT
    @Path("/shareClasse")
    @Produces(MediaType.APPLICATION_JSON)
    public String shareClasse(@FormParam("id") int id, @FormParam("uuid") String uuid, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.shareClasse(id, uuid).toString();
    }

    /**
     * Récupère un exercice du degré indiqué pour le jeu indiqué
     *
     * @param id l'id du jeu
     * @param degre le degre de l'exercice
     * @return l'exercice
     */
    @POST
    @Path("/getExercice")
    @Produces(MediaType.APPLICATION_JSON)
    public String getExercice(@FormParam("id") String id, @FormParam("degre") String degre) {
        return db.getExercice(id, degre).toString();
    }

    /**
     * Récupère un exercice forcé à travers son identifiant
     *
     * @param forcedExerciceId l'id de l'exercice à récupérer
     * @return l'exercice
     */
    @POST
    @Path("/getExerciceById")
    @Produces(MediaType.APPLICATION_JSON)
    public String getExerciceById(@FormParam("forcedExerciceId") int forcedExerciceId) {
        return db.getExerciceById(forcedExerciceId).toString();
    }

    /**
     * Obtient les informations d'un seul parcours à partir du userId et du
     * nomParcours (utile si on n'a pas l'id)
     *
     * @param parcours_id l'id du parcours
     * @return le parcours demandé parcours si tout va bien, le problème
     * autrement
     */
    @POST
    @Path("/getSingleParcoursById")
    @Produces(MediaType.APPLICATION_JSON)
    public String getSingleParcoursById(@FormParam("parcoursId") int parcours_id) {
        return db.getSingleParcoursById(parcours_id).toString();
    }

    /**
     * Obtient les informations d'un seul parcours à partir du userId et du
     * nomParcours (utile si on n'a pas l'id)
     *
     * @param user_id l'id de l'utilisateur / le prof
     * @param nomParcours le nom du parcours (unique pour chauqe userId)
     * @return le parcours demandé parcours si tout va bien, le problème
     * autrement
     */
    @POST
    @Path("/getSingleParcoursByUserAndName")
    @Produces(MediaType.APPLICATION_JSON)
    public String getSingleParcoursByUserAndName(@FormParam("userId") int user_id, @FormParam("nomParcours") String nomParcours) {
        return db.getSingleParcoursByUserAndName(user_id, nomParcours).toString();
    }
    
    /**
     * Obtient les étapes personnalisés d'un parcours pour un élève en particulier
     * @param student_id l'id de l'élève
     * @param parcours_id l'id du parcours
     * @return le parcours avec les étapes si tout va bien, le problème autrement
     */
    @POST
    @Path("/getStudentCustomStepsOfParcours")
    @Produces(MediaType.APPLICATION_JSON)
    public String getStudentCustomStepsOfParcours(@FormParam("studentId") int student_id, @FormParam("parcoursId") int parcours_id) {
        return db.getStudentCustomStepsOfParcours(student_id,parcours_id).toString();
    }

    /**
     * Vérifie que le parcours existe pour une classe ou un élève donné
     *
     * @param target élément ciblé : classe ou élève
     * @param target_id l'id de la classe ou de l'élève en question
     * @param parcours_id l'id du parcours
     * @return le parcours demandé (pour l'instant vide car pas besoin de plus)
     * parcours si tout va bien, le problème autrement
     */
    @POST
    @Path("/checkParcoursFor")
    @Produces(MediaType.APPLICATION_JSON)
    public String checkParcoursFor(@FormParam("target") String target, @FormParam("targetId") int target_id, @FormParam("parcoursId") int parcours_id) {
        return db.checkParcoursFor(target, target_id, parcours_id).toString();
    }

    /**
     * Vérifie combien de fois un niveau a été fait dans un parcours pour un
     * élève donné
     *
     * @param studentId l'id de l'élève
     * @param parcoursId l'id du parcours
     * @param exoId l'id de l'exercice
     * @return le nombre de lignes trouvé count si tout va bien, le problème
     * autrement
     */
    @POST
    @Path("/checkStepDoneForStudent")
    @Produces(MediaType.APPLICATION_JSON)
    public String checkStepDoneForStudent(@FormParam("studentId") int studentId, @FormParam("parcoursId") int parcoursId, @FormParam("exoId") int exoId) {
        return db.checkStepDoneForStudent(studentId, parcoursId, exoId).toString();
    }

    @POST
    @Path("/getExerciceEleves")
    @Produces(MediaType.APPLICATION_JSON)
    public String getExerciceEleves(@FormParam("id") int id, @FormParam("degre") int degre) {
        String retour = db.getExerciceEleves(degre, id).toString();
        return retour;
    }

    @POST
    @Path("/getExericesForValidation")
    @Produces(MediaType.APPLICATION_JSON)
    public String getExericesForValidation() {
        return db.getExericesForValidation().toString();
    }

    @POST
    @Path("/addExercice")
    @Produces(MediaType.APPLICATION_JSON)
    public String getExercice(@FormParam("exercice") String exSrc, @FormParam("fkJeu") int fkJeu, @FormParam("eleve") int eleveID, @FormParam("degre") int degre, @FormParam("forcedState") int forcedState) {
        return db.addExercice(exSrc, fkJeu, eleveID, degre, forcedState).toString();
    }

//    @POST
//    @Path("/getStepsInfo")
//    @Produces(MediaType.APPLICATION_JSON)
//    public String getStepsInfo(@FormParam("stepsId") String stepsId, @FormParam("salt") String salt) {
//        if (!Salt.compare(salt)) {
//            return invalidSaltResponse;
//        }
//        return db.getStepsInfo(stepsId).toString();
//    }
    @POST
    @Path("/getStudentParcoursStats")
    @Produces(MediaType.APPLICATION_JSON)
    public String getStudentParcoursStats(@FormParam("studentId") int studentId, @FormParam("parcoursId") int parcoursId, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.getStudentParcoursStats(studentId, parcoursId).toString();
    }

    @POST
    @Path("/getStatsEleve")
    @Produces(MediaType.APPLICATION_JSON)
    public String getStatsEleve(@FormParam("id") int id, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.getStatsEleve(id).toString();
    }

    @POST
    @Path("/getStatsClasse")
    @Produces(MediaType.APPLICATION_JSON)
    public String getStatsClasse(@FormParam("id") int id, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.getStatsClasse(id).toString();
    }

    @POST
    @Path("/addStats")
    @Produces(MediaType.APPLICATION_JSON)
    public String addStats(@FormParam("idEleve") int idEleve, @FormParam("idJeu") int idJeu, @FormParam("mode") int mode, @FormParam("exerciceId") int exercice_id, @FormParam("parcoursId") int parcoursId, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.addStats(idEleve, idJeu, mode, exercice_id, parcoursId).toString();
    }

    @POST
    @Path("/updateStats")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateStats(@FormParam("id") int id, @FormParam("evaluation") String evaluation, @FormParam("details") String details, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.updateStats(id, evaluation, details).toString();
    }

    @POST
    @Path("/newDATEntry")
    @Produces(MediaType.APPLICATION_JSON)
    public String newDATEntry(@FormParam("studentId") int studentId, @FormParam("exerciceId") int exerciceId, @FormParam("mode") int mode, @FormParam("parcoursId") int parcoursId, @FormParam("salt") String salt, @FormParam("datFunction") String datFunction) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.newDATEntry(studentId, exerciceId, mode, parcoursId, datFunction).toString();
    }

    @POST
    @Path("/updateDATEntry")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateDATEntry(@FormParam("id") int id, @FormParam("content") String content, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.updateDATEntry(id, content).toString();
    }

    @POST
    @Path("/updateStudentCollection")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateStudentCollection(@FormParam("studentId") int studentId, @FormParam("collectionData") String collectionData, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.updateStudentCollection(studentId, collectionData).toString();
    }

    /**
     * Récupère la collection d'un élève
     *
     * @param studentId l'id de l'élève en question
     * @param salt le salt de communication
     * @return la collection si tout se passe bien, le problème autrement
     */
    @POST
    @Path("/getStudentCollection")
    @Produces(MediaType.APPLICATION_JSON)
    public String getStudentCollection(@FormParam("studentId") int studentId, @FormParam("salt") String salt){
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.getStudentCollection(studentId).toString();
    }


    @GET
    @Path("/getUsers")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUsers() {
        return db.getUsers().toString();
    }

    @GET
    @Path("/getAllExercices")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllExercices() {
        return db.getAllExercices().toString();
    }

    @GET
    @Path("/getAllSharedParcours")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllSharedParcours() {
        return db.getAllSharedParcours().toString();
    }

    @GET
    @Path("/getAllPublicParcours")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllPublicParcours() {
        return db.getAllPublicParcours().toString();
    }

    @POST
    @Path("/updateRole")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateRole(@FormParam("userId") int userId, @FormParam("role") int role, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.updateRole(userId, role).toString();
    }

    @POST
    @Path("/signaler")
    @Produces(MediaType.APPLICATION_JSON)
    public String signaler(@FormParam("msg") String message, @FormParam("password") String password, @FormParam("ex") int exId, @FormParam("prof") int profId, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        if (db.checkPasswordForId(profId, password)) {
            return db.signaler(message, exId, profId).toString();
        } else {
            return new DataReturn<>(ReturnType.WARNING, "Mauvais mot de passe").toString();
        }
    }

    @POST
    @Path("/checkPasswordForId")
    @Produces(MediaType.APPLICATION_JSON)
    public String checkPasswordForId(@FormParam("password") String password, @FormParam("prof") int profId, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        if (db.checkPasswordForId(profId, password)) {
            return "true";
        } else {
            return "false";
        }
    }

    @POST
    @Path("/getExerciceSignale")
    @Produces(MediaType.APPLICATION_JSON)
    public String getExerciceSignale(@FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }

        return db.getExerciceSignale().toString();
    }

    @POST
    @Path("/getExericesById")
    @Produces(MediaType.APPLICATION_JSON)
    public String getExericesById(@FormParam("exId") int exId) {

        return db.getExericesById(exId).toString();
    }

    @POST
    @Path("/updateExercice")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateExercice(@FormParam("salt") String salt, @FormParam("exercice") String exercice, @FormParam("levelInput") String level, @FormParam("exId") int exId) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.updateExercice(exercice, level, exId).toString();
    }

    @POST
    @Path("/updateExerciceState")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateExerciceState(@FormParam("salt") String salt, @FormParam("description") String description, @FormParam("state") int state, @FormParam("levelInput") String level, @FormParam("imageB64") String imageB64, @FormParam("metaDataJson") String metaDataJson, @FormParam("exId") int exId) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.updateExerciceState(description, state, level, imageB64, metaDataJson, exId).toString();
    }

    /**
     * Ajoute une activité débranchée ou externe dans la base de données
     *
     */
    @POST
    @Path("/createUnpluggedActivity")
    @Produces(MediaType.APPLICATION_JSON)
    public String createUnpluggedActivity(@FormParam("salt") String salt, @FormParam("exercice") String exSrc, @FormParam("fkJeu") int fkJeu, @FormParam("eleve") int eleveID, @FormParam("exoName") String exoName) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.createUnpluggedActivity(exSrc, fkJeu, eleveID, exoName).toString();
    }

    /**
     * Met à jour le step dans la base de données (Modifie un exercice débranché
     * déjà existant)
     *
     * @param {int} exo_id l'id du step
     * @param {string} content le contenu à assigner au step
     */
    @POST
    @Path("/updateUnpluggedExercice")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateUnpluggedExercice(@FormParam("stepId") int exo_id, @FormParam("content") String exercice) {
        return db.updateUnpluggedExercice(exo_id, exercice).toString();
    }

    @POST
    @Path("/getStatsByEleveForGraphic")
    @Produces(MediaType.APPLICATION_JSON)
    public String getStatsByEleveForGraphic(@FormParam("salt") String salt, @FormParam("id") int id) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.getStatsByEleveForGrapic(id).toString();
    }

    @POST
    @Path("/deleteExercice")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteExercice(@FormParam("salt") String salt, @FormParam("exId") int id) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.deleteExercice(id).toString();
    }

    /**
     * Supprime le parcours juste pour un élève ou une classe
     *
     */
    @POST
    @Path("/deleteParcoursFrom")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteParcoursFrom(@FormParam("target") String target, @FormParam("targetId") int target_id, @FormParam("parcoursId") int parcours_id, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.deleteParcoursFrom(target, target_id, parcours_id).toString();
    }

    /**
     * Supprime un parcours à travers son Id
     *
     * @param parcours_id le nom du parcours à supprimer
     * @param salt le salt de communication
     * @return un DataReturn avec le String OK si tout se passe bien, le
     * problème sinon
     *
     */
    @POST
    @Path("/deleteParcoursWithId")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteParcoursWithId(@FormParam("parcoursId") int parcours_id, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.deleteParcoursWithId(parcours_id).toString();
    }

    /**
     * Réinitialise un parcours pour un élève en supprimant les historiques de
     * ses exercices dans le parcours
     *
     * @param studentId l'id de l'élève
     * @param parcoursId l'id du parcours
     * @param salt le salt de communication
     * @return un DataReturn avec le String OK si tout se passe bien, le
     * problème sinon
     */
    @POST
    @Path("/resetParcoursForStudent")
    @Produces(MediaType.APPLICATION_JSON)
    public String resetParcoursForStudent(@FormParam("studentId") int studentId, @FormParam("parcoursId") int parcoursId, @FormParam("salt") String salt) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.resetParcoursForStudent(studentId, parcoursId).toString();
    }

    @POST
    @Path("/getSprites")
    @Produces(MediaType.APPLICATION_JSON)
    public String getSprites(@FormParam("salt") String salt, @FormParam("gameId") int id) {
        if (!Salt.compare(salt)) {
            return invalidSaltResponse;
        }
        return db.getSprites(id).toString();
    }
}
