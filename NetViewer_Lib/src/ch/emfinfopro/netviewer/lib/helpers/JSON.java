package ch.emfinfopro.netviewer.lib.helpers;

import ch.emfinfopro.netviewer.lib.beans.DateTime;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import java.lang.reflect.Type;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 *
 * @author SchnieperN
 */
public class JSON {
    
    public static final String DEFAULT_DATE_FORMAT = "dd.MM.yyyy";
    private static final HashMap<Class, String> dateFormats = new HashMap<>();
    
    static {
        //declare date formats here
        dateFormats.put(DateTime.class, "dd.MM.yyyy HH:mm:ss");
    }
    
    public static String toJSON(Object o) {
        Gson gson = getGsonBuilder().create();
        return gson.toJson(o);
    }
    
    public static String toJSON(Object o, String defaultDateFormat) {
        Gson gson = getGsonBuilder(defaultDateFormat).create();
        return gson.toJson(o);
    }
    
    public static <T> T fromJSON(String json, Type type) {
        Gson gson = getGsonBuilder().create();
        return gson.fromJson(json, type);
    }
    
    public static <T> T fromJSON(String json, Type type, String defaultDateFormat) {
        Gson gson = getGsonBuilder(defaultDateFormat).create();
        return gson.fromJson(json, type);
    }
    
    private static GsonBuilder getGsonBuilder() {
        return getGsonBuilder(DEFAULT_DATE_FORMAT);
    }
    
    private static GsonBuilder getGsonBuilder(String defaultDateFormat) {
        GsonBuilder gb = new GsonBuilder().setDateFormat(defaultDateFormat);
        for(Entry<Class, String> dateFormat : dateFormats.entrySet()) {
            gb.registerTypeAdapter(dateFormat.getKey(), new DateSerializer<>(dateFormat.getValue()));
            gb.registerTypeAdapter(dateFormat.getKey(), new DateDeserializer<>(dateFormat.getValue(), dateFormat.getKey()));
        }
        return gb;
    }
    
    private static class DateSerializer<T extends Date> implements JsonSerializer<T> {

        private String dateFormat;

        private DateSerializer(String dateFormat) {
            this.dateFormat = dateFormat;
        }
        
        @Override
        public JsonElement serialize(T date, Type type, JsonSerializationContext jsc) {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            return new JsonPrimitive(sdf.format(date));
        }
        
    }
    
    private static class DateDeserializer<T extends Date> implements JsonDeserializer<T> {

        private String dateFormat;
        private final Class<T> clazz;

        public DateDeserializer(String dateFormat, Class<T> clazz) {
            this.dateFormat = dateFormat;
            this.clazz = clazz;
        }

        @Override
        public T deserialize(JsonElement element, Type arg1, JsonDeserializationContext context) throws JsonParseException {
            String dateString = element.getAsString();
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            try {
                T date = clazz.newInstance();
                date.setTime(sdf.parse(dateString).getTime());
                return date;
            } catch (InstantiationException | IllegalAccessException | ParseException e) {
                throw new JsonParseException(e.getMessage(), e);
            }
        }
    }
    
}
