/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.netviewer.lib.salt;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author carre
 */
public class Salt {

    private final static String PASSWORD_P1 = "THIS_IS_THE_FIRST_PASSWORD";
    private final static String PASSWORD_P2 = "THIS_IS_THE_SECOND_PASSWORD";

    public static String getSalt() {
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE", Locale.ENGLISH);
        String day = formatter.format(new Date());
        return encrypt(PASSWORD_P1 + day + PASSWORD_P2);
    }
    
    public static boolean compare(String salt){
        return getSalt().equals(salt);
    }

    public static String encrypt(String msg) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] passBytes = msg.getBytes();
            md.reset();
            byte[] digested = md.digest(passBytes);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < digested.length; i++) {
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
            return sb.toString();
        } catch (Exception ex) {
        }
        return null;
    }
}
