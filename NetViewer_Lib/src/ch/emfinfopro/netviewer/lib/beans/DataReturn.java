/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.emfinfopro.netviewer.lib.beans;

import ch.emfinfopro.netviewer.lib.helpers.JSON;
import com.google.gson.reflect.TypeToken;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author carre
 */
public class DataReturn<T> {

    private ReturnType status = ReturnType.FAIL;
    private String message;
    private T data;
    private String[] affectedFields;
    private String[] affectedTables;
    private Integer errorCode;
    
    public DataReturn(String inJSON, String dataClass) {
        //TODO mettre dans classe JSON
        DataReturn dr = null;
        if(dataClass != null) {
            switch (dataClass) {
                case "":
                    dr = JSON.fromJSON(inJSON, new TypeToken<DataReturn>() {
                    }.getType());
                    break;
                case "User":
                    dr = JSON.fromJSON(inJSON, new TypeToken<DataReturn<User>>() {
                    }.getType());
                    break;
                case "List<User>":
                    dr = JSON.fromJSON(inJSON, new TypeToken<DataReturn<List<User>>>() {
                    }.getType());
                    break;
                case "Info":
                    dr = JSON.fromJSON(inJSON, new TypeToken<DataReturn<Info>>() {
                    }.getType());
                    break;
            }
        }

        if (dr != null) {
            status = dr.status;
            message = dr.message;
            data = (T) dr.getData();
            affectedFields = dr.affectedFields;
            affectedTables = dr.affectedTables;
            errorCode = dr.errorCode;
        }
    }

    public DataReturn(ReturnType success, String message, T data) {
        this.status = success;
        this.message = message;
        this.data = data;
    }

    public DataReturn(ReturnType success, String message) {
        this.status = success;
        this.message = message;
    }

    public DataReturn(ReturnType success) {
        this.status = success;
    }

    public DataReturn(ReturnType success, T data) {
        this.status = success;
        this.data = data;
    }

    public DataReturn(boolean success, String message, T data) {
        this.status = success ? ReturnType.SUCCESS : ReturnType.FAIL;
        this.message = message;
        this.data = data;
    }

    public DataReturn(boolean success, String message) {
        this.status = success ? ReturnType.SUCCESS : ReturnType.FAIL;
        this.message = message;
    }

    public DataReturn(boolean success) {
        this.status = success ? ReturnType.SUCCESS : ReturnType.FAIL;
    }

    public DataReturn(boolean success, T data) {
        this.status = success ? ReturnType.SUCCESS : ReturnType.FAIL;
        this.data = data;
    }

    public DataReturn(T data) {
        this.data = data;
    }

    public DataReturn() { }

    public DataReturn<T> success(String message, T data) {
        status = ReturnType.SUCCESS;
        this.message = message;
        this.data = data;
        return this;
    }

    public DataReturn<T> successMessage(String message) {
        status = ReturnType.SUCCESS;
        this.message = message;
        return this;
    }

    public DataReturn<T> success(T data) {
        this.data = data;
        status = ReturnType.SUCCESS;
        return this;
    }

    public DataReturn<T> success() {
        status = ReturnType.SUCCESS;
        return this;
    }
    
    public DataReturn<T> noData(){
        status = ReturnType.NO_DATA;
        return this;
    }

    public DataReturn<T> fail(String message, T data, Integer errorCode) {
        status = ReturnType.FAIL;
        this.data = data;
        this.message = message;
        this.errorCode = errorCode;
        return this;
    }

    public DataReturn<T> fail(String message, Integer errorCode) {
        status = ReturnType.FAIL;
        this.message = message;
        this.errorCode = errorCode;
        return this;
    }
    
    public DataReturn<T> fail(String message) {
        status = ReturnType.FAIL;
        this.message = message;
        return this;
    }
    
    public DataReturn<T> exception(Exception ex) {
        status = ReturnType.EXCEPTION;
        if (ex instanceof SQLException) {
            status = ReturnType.DATABASE_ERROR;
            this.errorCode = ((SQLException) ex).getErrorCode();
        }
        this.message = ex.toString();
        return this;
    }

    public DataReturn<T> fail() {
        status = ReturnType.FAIL;
        return this;
    }

    public boolean isSuccess() {
        return status == ReturnType.SUCCESS;
    }
    
    public boolean isFail() {
        return status == ReturnType.FAIL;
    }
    
    public boolean isException() {
        return status == ReturnType.EXCEPTION;
    }
    
    public boolean isDatabaseError() {
        return status == ReturnType.DATABASE_ERROR;
    }

    public String getMessage() {
        return message;
    }

    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public ReturnType getStatus() {
        return status;
    }

    public void setStatus(ReturnType status) {
        this.status = status;
    }

    public String[] getAffectedFields() {
        return affectedFields;
    }

    public void setAffectedFields(String... affectedFields) {
        this.affectedFields = affectedFields;
    }

    public String[] getAffectedTables() {
        return affectedTables;
    }

    public void setAffectedTables(String... affectedTables) {
        this.affectedTables = affectedTables;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        return toJSONString();
    }

    public String toJSONString() {
        return JSON.toJSON(this);
    }

    public DataReturn<T> importFromJSON(String json) {
        return JSON.fromJSON(json, DataReturn.class);
    }
    
    public DataReturn<T> importFromJSON(String json, String datePattern) {
        return JSON.fromJSON(json, DataReturn.class, datePattern);
    }

    public enum ReturnType {
        SUCCESS, WARNING, NO_DATA, FAIL, INFO, SALT, EXCEPTION, DATABASE_ERROR;
    }

}
